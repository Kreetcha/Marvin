function fgosScrape(db, client, config){

  function dmAdmin(msg)
  {
    try {
      client.users.fetch(config.adminId)
      .then(user => {
        user.send(msg)

      }, reason => {
        console.log("ERR: fetching admin user from adminId @ "+ new Date());

      });

    } catch (err) {
      console.log("ERR: fgosScrape dmAdmin failed >.< @ "+ new Date());
      console.log("MSG: ");
      console.log(msg);
      console.log(); // \n
      console.log("ERR:");
      console.log(err);

    }
  }


  // dmAdmin("derp");
  console.log("=================reddiAlert (fgos)==================");
  console.log("fgosScrape running @ "+new Date());
  const request = require('request')
  const Discord = require('discord.js');
  const options = {
  	url: 'https://www.reddit.com/r/FreeGamesOnSteam/new.json',
  	method:"GET",
  	headers: {
  		'Accept': 'application/json',
  		'Accept-Charset': 'utf-8',
  		'User-Agent': 'my-reddit-client'
  	}
  }
  //trace logs
  // from https://stackoverflow.com/questions/45395369/how-to-get-console-log-line-numbers-shown-in-nodejs
  // var log = console.log;
  // console.log = function() {
  //     log.apply(console, arguments);
  //     // Print the stack trace
  //     console.trace();
  // };

  if (typeof db == "undefined") {
    console.log("db not connected (undefined)!!")
    console.log("ERROR: error retrieving fgos reddit @ "+new Date());
    // msg.author.send(msg.author+"db not connected ):\n Please retry or contact admin if this persists")
    return
  }
  //  request(options, { json: true }, (err, res, body) => {
  request(options, (err, res, body) => {
  	//  request('https://www.reddit.com/r/redditdev/top.json', { json: true }, (err, res, body) => {


    /////////////////////////////////
    //!!!!!!!! HTTP ERROR HANDLER IS WRITTEN WITH PRIORITY TOWARDS DEBUGGING AND NOT EFFIENCY !!!!!!!!!!!!!!!!!!!!!!
    // needs to be REDONE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  	if (err) {
      console.log("ERROR: request for fgos reddit json failed! @ "+ new Date());
      console.log(err);
      return
    }
    // HTTP ERROR DEBUG
    console.log("GET request for fgos response status("+ (typeof res.statusCode)+"): "+res.statusCode);

    if (res.statusCode == 503) {
      console.log("GET for fgos json returned err status 503\nskipping scrape...");
      //temp alert admin of 503 handliing
      dmAdmin("redditAlert fgos scrape: 503 server error @ "+ new Date())
      //lazy debug
      setTimeout(() => {

          dmAdmin(`${client.user.username} has defeated error 503! :smiling_imp:\n@ `+ new Date());
          console.log("Survived 503 error @ " + new Date());

      }, 30000);


      return
    } else if (res.statusCode != 200){
      console.log(`GET for fgos json returned UNKNOWN err status: ${res.statusCode}\nskipping scrape...`);
      dmAdmin(`redditAlert fgos scrape UNKNOWN HTTP ERROR: ${res.statusCode} server error @ `+ new Date())

      setTimeout(() => {

          dmAdmin(`${client.user.username} has defeated UNKOWN HTTP ERROR [${res.statusCode}] ! :smiling_imp:\n@ `+ new Date());
          console.log(`Survived unknown http error${res.statusCode} error @ `+ new Date());

      }, 30000);

      return
    }
  	// console.log(err);
  	//    console.log("res:");
  	//  console.log(res);
  	//console.log(body.url);
  	// console.log(body.explanation);
  	//console.log(res.body)

    //parse json to get list of post objs
  	var listObj = JSON.parse(res.body)
    if (typeof listObj.data == "undefined") {
      console.log("ERROR: request to fgos json didn't result in error but has no data :( @ "+ new Date() );
      dmAdmin("ERROR: request to fgos json didn't result in error but has no data :( @ "+ new Date() )
      return
    }
  	//console.log(JSON.stringify(listObj,0,2))
  	var postsCount = listObj.dist
    var whiteList = [
      "store.steampowered.com",
      "humblebundle.com"
    ]
  	// console.log(typeof listObj.data.children)
    //itterate over array of posts
  	for (var post of listObj.data.children){
      // if (whiteList.includes(post.data.domain) && post.data.link_flair_css_class != "ended") {
      // if (whiteList.includes(post.data.domain)) {
      //checking css link flair for expired free games
      // null link flair css class should mean a valid offer
      if (whiteList.includes(post.data.domain) && ( (post.data.link_flair_css_class === null) || (!post.data.link_flair_css_class.includes("ended")) ) ) {

        // console.log("=================reddiAlert (fgos) results==================");
        // console.log(post)
        var myData = {
          title: post.data.title,
          author: post.data.author,
          domain: post.data.domain,
          thumbnail: post.data.thumbnail,
          postID: post.data.id,
          link_flair_css_class: post.data.link_flair_css_class,
          content: post.data.selftext,
          linkToPost: post.data.permalink,
          linkFromPost: post.data.url,
          created_utc: post.data.created_utc

        }
        // console.log("myData: "+myData);



        //check if post is in db
        var postDbCheckCallback = function(err, rez) {
          // var thisPostDoc = {}
          //grab post doc from bind?
          var thisPostDoc = this.postDoc
          var channelsToPostOn = []
          //check if dbfind returned a doc
          if (rez.length == 0) {
            client.guilds.cache.forEach((guild) =>{
              // guildNames.push(guild.name);
              // console.log("============================");
              // console.log("Guild: "+guild.name);
              guild.channels.cache.forEach((channel) =>{
                if ((channel.type == "GUILD_TEXT") && (channel.name == "fgos" || channel.name == "freegamesonsteam") && (channel.permissionsFor(guild.me).has("SEND_MESSAGES"))) {
                  channelsToPostOn.push(channel)
                  // console.log("channel.id: "+channel.id);
                }
              });
            });
            // console.log("channelsToPostOn: "+channelsToPostOn);
            let promiseArr = channelsToPostOn.map(function(channel){
              let embedToSend = new Discord.MessageEmbed()
                .setTitle(thisPostDoc.title)
                .setURL("https://www.reddit.com"+thisPostDoc.linkToPost)
                .addField('Domain', thisPostDoc.domain, true)
                .addField('Offer', thisPostDoc.linkFromPost, true)
                .setDescription(thisPostDoc.content)
                // .setColor('#df0000')
                .setColor('#00df00')
              //return channel.send(embedToSend).then((sentMsg) => {

		if(thisPostDoc.thumbnail != 'default') {

                  embedToSend.setThumbnail(thisPostDoc.thumbnail)
		}

		return channel.send({embeds: [embedToSend]}).then((sentMsg) => {

	                return {"guildId":sentMsg.guild.id, "channelId":sentMsg.channel.id, "msgId":sentMsg.id}
              });
            });
            Promise.all(promiseArr).then((results) =>{
              // console.log("Sent msg ids??:");
              // console.log(results);
              //NOW POST DOC TO DB!! (:
              let dbDoc = {
                type:"freeGamesOnSteam",
                discordAlertMsgs: results,
                data: thisPostDoc
              }
              // console.log(dbDoc);
              db.collection('redditAlert').insertOne(dbDoc, (err,res) =>{
                if (err) {
                  console.log("ERROR: fgos post save failed!! @ "+new Date());
                  console.log(err);
                  // return {"ok":false, "err":err}
                }
                // console.log("Post doc Saved to db!!");
                console.log(`fgos NEW Post doc Saved to db! postID: ${res.ops[0].data.postID}`);

                 // else {
                 //   return {"ok":true}
                 // }
              });
            }).catch((err) =>{
              console.log("ERROR: in promiseArr in redditAlert @ "+new Date());
              console.log(err);
            });
          } else if (rez.length == 1) {
            //post already in db
            console.log(`fgos Post Already in db! postID: ${thisPostDoc.postID}`);
            //check if db post was not ended but new post is ended
            var newPostCssClass = thisPostDoc.link_flair_css_class
            var oldPostCssClass = rez[0].data.link_flair_css_class;
            if ((newPostCssClass != oldPostCssClass) && (newPostCssClass == "ended")) {
              //^^ need a better check for this
              //now edit post
              console.log(`fgos offer has now ended!! postID: ${rez[0].data.postID}`);
              rez[0].discordAlertMsgs.forEach((msgIds) =>{
                // var msgToEdit = client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).messages.get(msgIds.msgId)
                var editMsgCallback = function(postMsg){
                  // console.log("~~~~~~~~~~~~~~~~");
                  // console.log(postMsg.embeds);
                  const oldEmbed = postMsg.embeds[0]
                  const oldTitle = oldEmbed.title
                  const newTitle = "~~"+oldTitle+" (ENDED)~~"
                  const  newEmbed = new Discord.MessageEmbed(oldEmbed)
                  .setColor("#df0000")
                  .setTitle(newTitle)
                  .setColor("#df0000")
                   // console.log(newEmbed);
                  postMsg.edit(newEmbed);
                  // console.log(this.postDoc);

                }
                client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).fetchMessage(msgIds.msgId).then(editMsgCallback.bind({"postDoc": thisPostDoc}))



              });
              db.collection('redditAlert').updateOne({"data.postID": rez[0].data.postID}, {$set:{"data.link_flair_css_class": "ended"}}, (err,res) =>{
                console.log("fgos ended event, db updated! @ "+new Date());
              });

            }
            // console.log(rez[0]);
            // console.log("post already in db!!");
          } else if (rez.length > 1) {
            //DUPLICATE DOCUMENT ERROR!!!!!
            console.log("ERROR: DUPLICATE fgos DOCUMENT ERROR!!!!! @ "+new Date());
          }
        }

        // postDbCheckCallback.bind({"postDoc": myData})
        db.collection('redditAlert').find({"data.postID": post.data.id}).toArray(postDbCheckCallback.bind({"postDoc": myData}))

      }


  	}

  });

  return
}


module.exports.fgosScrape = fgosScrape;
