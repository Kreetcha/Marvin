


// respond to a roll call
module.exports.respond = function(msg){
  //console.log(msg.react);
  msg.react('\u26A1')
};

var tempIgnoreIDs = [];

// initiate a roll call
module.exports.doRollCall = function(config, client, msg){
  //first get the roll call channel
  // SETUP set id of guild to do rollcall in
  // create a text channel called "rollcall" inside of it
  // var rollCallGuild = client.guilds.find('name', config.rollCallGuildID);

  //add cmd to list rollcall later?
  // if (config.rollCallIDs.length == 0) return;
  //rollCall
  var rollCallChannelToSendTo;

  //skip if no users on rollCall List

  if (msg) {
    //if roll call was manually requested for a bot to perform, set channel to do roll call to that channel

    if (config.rollCallIDs.length == 0) {
      msg.channel.send("ERR: No bots are on my rollCall list >.<");
      return;
    }

    if (msg.guild === null || msg.guild.id != config.rollCallGuildID) {
      msg.channel.send("ERR: current guild/channel is not a valid place to rollCall :man_facepalming:")
      return;
    }
    rollCallChannelToSendTo = msg.channel;
  } else {
    //auto run rollcall, set roll call channel to channel in config

    //skip if no users on rollCall List
    if (config.rollCallIDs.length == 0) return;

    var rollCallGuild = client.guilds.find(guild => guild.id == config.rollCallGuildID);

    if (rollCallGuild == null) {
      client.users.get(config.adminId).send(`ERROR: doRollCall - rollCallGuild not found ID[${config.rollCallGuildID}]`+ new Date() );
      return;
    }

    // var rollCallChannel = rollCallGuild.channels.find('name', 'rollcall');
    var rollCallChannel = rollCallGuild.channels.find(channel => channel.name == 'rollcall');


    if (rollCallChannel == null) {
      client.users.get(config.adminId).send(`ERROR: doRollCall - rollCallChannel not found in guild: ID[${config.rollCallGuildID}]`+ new Date() );
      return;
    }
    //make sure roll call channel is valid and if bot has permissions to send on it
    // if ((channel.type == "text") && (channel.name == "fgos" || channel.name == "freegamesonsteam") && (channel.permissionsFor(guild.me).has("SEND_MESSAGES"))) {


      if ( !((rollCallChannel.type == "GUILD_TEXT") && (rollCallChannel.name == "rollcall") && (rollCallChannel.permissionsFor(rollCallGuild.me).has("SEND_MESSAGES"))) ){
        //need to make this more verbose later
        client.users.get(config.adminId).send(`ERROR: doRollCall - rollCallChannel not valid or bad bot permissions guild: ID[${config.rollCallGuildID}]`+ new Date() );
        return
      }
    rollCallChannelToSendTo = rollCallChannel
  }

  // rollCallChannel.send('[rollcall]')

  //collector filter
  const filter = (reaction, user) => {
	   // return reaction.emoji.name === '👌' && user.id === message.author.id;
    return true
  };
  //choosing non promise bassed reaction collector


  // send and start roll call

  rollCallChannelToSendTo.send('[rollCall]').then((sentRollCallMsg) => {
    const collector = sentRollCallMsg.createReactionCollector(filter, {time:10000})
    // collector.on('collect', (reaction, reactionCollector) => {
    //   console.log(`Collected ${reaction.emoji.name}`);
    //   // console.log(reaction);
    //
    // });

    //event for end of collector timout
    collector.on('end', collected => {
      // console.log(`Collected ${collected.size} items`);
      // console.log("====");
      // console.log(collected);
      // console.log("====");

      //create local copy of atendanceList here
      // var aList = config.rollCallIDs;
      //this fixes above method causing aList to be a pointer to the config file array
      var aList = JSON.parse(JSON.stringify(config.rollCallIDs))

      //REMOVE TEMPORARY IGNORED IDS HERE
      for (const idToIgnore of tempIgnoreIDs) {
        aList.splice(aList.indexOf(idToIgnore),1)
      }

      collected.forEach((reaction) => {
        // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        // console.log(reaction);

        // console.log(reaction.users);
        reaction.users.forEach((user) =>{
          // console.log(`${user.username} id:${user.id} type:${typeof user.id}`);

          //check if user id is in antendanceList array
          var aListIDIndex = aList.indexOf(user.id);

          //random question, is indexOf != 1 better than indexOf > -1
          if (aListIDIndex > -1) {
            //user id of reaction as in aList!


            //remove id from aList
            aList.splice(aListIDIndex,1);
            // var removedID = aList.splice(aListIDIndex,1);

          }
        });
        //end of itterating user collection for reaction

      });
      // end of itterating reaction collection

      //check if all IDs were found and removed from attendance list
      var missingCount = aList.length;
      if (missingCount != 0 ) {
        //users in alist did not respond!

        //handle grammar here for multiple bots
        // var botStr;
        // if (missingCount > 1) {
        //   botStr = `BOTS OFFLINE(${missingCount})`
        // } else {
        //
        // }
        // var botStr = (missingCount > 1) ? `BOTS OFFLINE(${missingCount})`:``;
        var botStr = (missingCount > 1) ? `BOTS OFFLINE(${missingCount})`:`BOT OFFLINE`;

        //generate list of botNames from IDS
        offlineBotsStrs = [];
        for (const botID of aList) {
          //get bot data

          var offBot = client.users.get(botID);
          offlineBotsStrs.push(`${offBot.username.padEnd("14",".")}${botID}`);
        }

        var msgToSend = `\`!!!${botStr}!!! @`+ new Date()+"`\n```"+offlineBotsStrs.join("\n")+"\n```"
        if (msg) {
          //if roll call was manually requested for a bot to perform, reply in same channel with results
          // msg.reply(msgToSend)
          msg.channel.send(msgToSend)

          // client.users.get(config.adminId).send(msgToSend);


        } else {
          rollCallChannelToSendTo.send(msgToSend);
          client.users.get(config.adminId).send(msgToSend);

        }


      } else {
        if (msg) {
          //if roll call was manually requested for a bot to perform, reply in same channel with results
          // msg.reply('All bots are accounted for');
          msg.channel.send('All bots are accounted for');

        }
        // else {
        //
        //   // /console.log("all bots are here (=");
        // }
      }
    });

  });


}


module.exports.tempIgnore = function(idToIgnore){
  tempIgnoreIDs.push(idToIgnore)
}

module.exports.rollcallignoreCmd = function(config, client, msg, cmdArgs){
  if (cmdArgs.length == 0) {
    msg.channel.send("Please specify a user id to ignore during rollCalls...\nother commands: list, clearall, clear one")
    return;
  // } else if (cmdArgs.length > 1) {
    // msg.channel.send("ERR: Too many arguments!\nPlease specify a user id to ignore during rollCalls...")
    // return;

  } else if (cmdArgs[0] == "list") {
    if (tempIgnoreIDs.length == 0) {
      msg.channel.send("Currently not ignoring any bots from rollCall list")
      return

    } else {
      var ignoreListStr = [];
      for (const ignoreID of tempIgnoreIDs) {
        //get bot data

        var ignoredBot = client.users.get(ignoreID);
        ignoreListStr.push(`${ignoredBot.username.padEnd("14",".")}${ignoreID}`);
      }
      msg.channel.send(`\`Ignoring ${tempIgnoreIDs.length} bots:\`\n\`\`\`${ignoreListStr.join("\n")}\n\`\`\``)
      return
    }
  } else if (cmdArgs[0] == "clearall") {
    tempIgnoreIDs = [];
    msg.channel.send("rollCall ignore list cleared!");

  } else if (cmdArgs[0] == "clearone") {
    if (cmdArgs.length != 2) {
      msg.channel.send("Please specify the userID to remove from ignore list");
      return;
    } else {
      var idToRemove = cmdArgs[1];
      var idToRemoveIndex = tempIgnoreIDs.indexOf(idToRemove);

      //random question, is indexOf != 1 better than indexOf > -1
      if (idToRemoveIndex > -1) {
        //user id of reaction as in aList!


        //remove id from aList
        tempIgnoreIDs.splice(idToRemoveIndex,1);
        // var removedID = aList.splice(aListIDIndex,1);
        msg.channel.send(`Removed userID [${idToRemove}] from the the ignore list!` )
        return;
      } else {
        msg.channel.send(`ERR: userID [${idToRemove}] was not on the ignore list` )
        return;

      }


    }

  } else if (cmdArgs.length == 1){
    //check if userid returns a valid user on the rollCallIDs list
    var idToIgnore = cmdArgs[0]
    // var userToIgnore = client.users.get(idToIgnore);
    // if (userToIgnore === undefined) {
    //   msg.channel.send(`ERR: userID ${idToIgnore} did not return a valid user` )
    //   return
    // } else
    if (!(config.rollCallIDs.includes(idToIgnore))) {
      msg.channel.send(`ERR: userID [${idToIgnore}] was not on the rollCall list` )
      return;

    } else {
      var userToIgnore = client.users.get(idToIgnore);
      // if (userToIgnore === undefined) {

      tempIgnoreIDs.push(idToIgnore)
      msg.channel.send(`Removed bot from rollCall until this bot reboots: ${userToIgnore.username}` )
    }
  }
}
