const util = require('discord.js').Util

var dClient = null;
var dConfig = null;

module.exports.init = function(dClientIn, dConfigIn) {

  if(!dClientIn) {
    console.log("ERR: dbot init failure, dClient invalid");
    return false;
  }
  dClient = dClientIn;
  if(!dConfigIn) {
    console.log("ERR: dbot init failure, dConfig invalid");
    return false;
  }
  dConfig = dConfigIn;
  return true;

}


module.exports.dmAdmin = function(msgToAdmin) {
  var adminId = dConfig.adminId

  try {
    dClient.users.fetch(adminId)
    .then(user => {
      user.send(msgToAdmin)

    })

  } catch (err) {
    console.log("ERR: dbot dmAdmin failed >.<");
    console.log("ERR:");
    console.log(err);
    return false;
  }
  return true;
}

// module.exports.sendMessageSplit = async function(channel, splitOptions, msgStr ) {
// 	console.log(splitOptions);
// 	chunksToSend = util.splitMessage(msgStr, splitOptions)
// 	console.log("chunksToSend")
// 	console.log(chunksToSend)
//
// 	for (thisChunk of chunksToSend){
// 		await channel.send(thisChunk)
// 	}
//
// }

// function splitMessage(msgStr, splitOptions) {
// 	// returns array of split msg chunks
// 	// splitOptions = {
// 	// 	"append":"",
// 	// 	"prepend":"",
// 	// 	"maxLength":"",
// 	// 	"splitChar": ["\n"] //defaults to \n, \t, \s, ".", splitAtMaxLength in that order
// 	// }
// 	var defaultMaxLength = 1900;
// 	var defaultSplitChars = ['\n', '\t', '\s' ];
//
// 	var maxLength = ("maxLength" in splitOptions) ? splitOptions.maxLength : defaultMaxLength;
// 	var splitChars = ("splitChars" in splitOptions) ? splitOptions.splitChar : defaultSplitChars;
//
// 	var msgLength = mstStr.length;
// 	if(msgLength <= maxLength) return [msgLength]
// 	var msgToSplit = msgStr;
// 	var stillChunksLeft = True;
//
// 	var msgChunks = []
// 	while (stillChunksLeft) {
// 		// check if mstToSplit is shorter than maxLength
// 		if(msgToSplit <= maxLength) {
// 			//completed splitting!
// 			if (msgToSplit.length !== 0) {
// 				//add last chunk to array
// 				msgChunks.push(msgToSplit)
//
// 			}
// 			// COMPLETE
// 			return msgChunks
// 		}
//
// 		// try to split off next chuck of text from beginning of long message
// 		for (char of splitChars) {
//
// 			//check if this char is in msg
// 			// if not skip it
// 			if(msgToSplit.includes(char) == False) continue;
//
// 			// attempt to split with this char
// 			var msgSplitAttempt = msgToSplit.split(char);
//
// 			// if(msgSplitAttempt)
// 			//find largest chunk using this char that is less than maxLength
// 			var chunkAttemptIndex = 0;
// 			var lastchunAttemptIndex = 0;
// 			var splitResult = False;
//
// 			while(1) {
// 				//
// 				if(chunkAttemptIndex >=  msgSplitAttempt.length) {
// 					// hit end of chunk array
// 					if (msgToSplit()) {
//
// 					}
// 				}
// 				var chunkAttemptIndex = msgToSplit.split(char)[chunkAttemptIndex];
//
//
//
// 				splitAttemptIndex++;
// 			}
// 			//attempt to pull split chunk from beginning
// 			// var splitAttempt = msgToSplit.split(char)[0]
// 			//see if chunk is not larger than max length
// 			if ( splitAttempt.length <= maxLength) {
// 				// found valid split
//
// 				// add chunk to array
// 				msgChunks.push(splitAttempt)
//
// 				// remove chunk from msgToSplit
//
//
// 			} else {
//
// 			}
// 		}
//
//
// 		// try to split off next chuck of text from beginning of long message
// 		// for (char of splitChars) {
// 		// 	//attempt to pull split chunk from beginning
// 		// 	var splitAttempt = msgToSplit.split(char)[0]
// 		// 	//see if chunk is not larger than max length
// 		// 	if ( splitAttempt.length <= maxLength) {
// 		// 		// found valid split
// 		//
// 		// 		// add chunk to array
// 		// 		msgChunks.push(splitAttempt)
// 		//
// 		// 		// remove chunk from msgToSplit
// 		//
// 		//
// 		// 	} else {
// 		//
// 		// 	}
// 		// }
// 	}
//
//
// }



module.exports.sendMessageSplit = async function(channel, splitOptions, msgStr ) {
	console.log(splitOptions);
	var chunksToSend = util.splitMessage(msgStr, splitOptions);
	console.log("chunksToSend");
	console.log(chunksToSend);

	for (thisChunk of chunksToSend){
		await channel.send(thisChunk);
	}

}

module.exports.replyMessageSplit = async function(msg, splitOptions, msgStr ) {
	console.log(splitOptions);
	var chunksToSend = util.splitMessage(msgStr, splitOptions);
	console.log("chunksToSend");
	console.log(chunksToSend);

	for (thisChunk of chunksToSend){
		await msg.reply(thisChunk);
	}

}



module.exports.areYouSure = async function(msg, promptStr, time, callback, timeoutCallback) {
//??? https://stackoverflow.com/questions/57452056/discord-js-how-to-send-a-message-and-collect-reactions-from-it
  // console.log("ARE YOU SURE TEST:");
  var respondantId = msg.author.id;
  // console.log("respondantId: " + respondantId);
  msg.reply("Loading menu...")
  .then(async function (message) {

    var yesEmoji = String.fromCharCode(9989); // white check mark (looks green in discord)
    var noEmoji = String.fromCharCode(10060); // red X emoji
    await message.react(yesEmoji)
      .then(async function() { await message.react(noEmoji)})
      .then(() => {message.edit(promptStr)});

    const filter = (reaction, user) => {
      if(user.id != respondantId) return false;
      // console.log("respondantId: " + respondantId);
      //
      // console.log("filter user.id: " + user.id);
      // console.log("reaction.emoji: " + reaction.emoji);
      // console.log("yesEmoji: " + yesEmoji);
      // console.log("reaction.emoji.name: "+ reaction.emoji.name);

      if(reaction.emoji.name == yesEmoji || reaction.emoji.name == noEmoji) return true;
      return false;// filter for only the user the prompt is for and for the correct emojis
    };

    const collector = message.createReactionCollector(filter, {time: time });

    collector.on('collect', (reaction, user) => {
      // do stuff
      if(reaction.emoji.name === yesEmoji) {
        callback(true);
        collector.stop("success");
        return;

      } else if(reaction.emoji.name == noEmoji) {
        callback(false);
        collector.stop("success");
        return;
      } else {


      }

    });

    collector.on('end',(collected, reason) => {
      if(reason == "success"){
        // console.log("collector success! ^.^");
        return;
      } else {
        // console.log("collectorEndReason: "+reason);
        // list of collectorEndReason strings i have found
        // "time",
        timeoutCallback(reason, message);
        return;
      }

    });
  });


}


// var chooseFromListDefault = [
//   ":one:",
//   ":two:",
//   ":three:",
//   ":four:",
//   ":five:",
//   ":six:",
//   ":seven:",
//   ":eight:",
//   ":nine:",
//   ":ten:"
//   // "",
//   // "",
//
// ];


// 0-9 keycap emojis
var reaction_numbers = ["\u0030\u20E3","\u0031\u20E3","\u0032\u20E3","\u0033\u20E3","\u0034\u20E3","\u0035\u20E3", "\u0036\u20E3","\u0037\u20E3","\u0038\u20E3","\u0039\u20E3"];




module.exports.chooseFromList = function(msg, promptStr, listStrs, time_ms, callback, timeoutCallback) {
  // REJECT IF LESS THAN 2 CHOICESS!!!!!
  // if asdasd
  //load from preset list

  // var forward_emoji = "";
  // var back_emoji = "";
  var cancelEmoji = String.fromCharCode(10060);
  var listEmojis = reaction_numbers;

  var respondantId = msg.author.id;

  // if (listStrs > listEmojis.length) {
  //   //will need to make multiple sets of ten
  //
  //
  //
  // } else {
  //
  //
  //
  // }

  //form prompt message with list

  //generate list with emojis
  var promptListStr = "";
  for (var i = 0; i < listStrs.length; i++) {
    thisChoiceStr = listEmojis[i] + " - "+ listStrs[i] + "\n";
    promptListStr += thisChoiceStr;
  }

  // var promptMessageString = promptStr + "\n" + promptListStr;
  var promptMessageString = promptStr + "\n\n" + promptListStr + "\n"+ cancelEmoji + " - Cancel\n\n";



  async function reactInOrder(message, reactArr, amount, index) {

    // var thisReaction = dClient.emojis.cache.find(emoji => emoji.name === reactArr[index]);
    var thisReaction = reactArr[index];

    index++;
    // console.log("thisReaction:");
    // console.log(thisReaction);
    await message.react(thisReaction)
    .then(async function () {
      if(index >= amount || index >= reactArr.length) {
        return; //done
      } else {
        await reactInOrder(message, reactArr, amount, index);

      }

    });
  };

  var listEmojisIndex = 0;
  msg.reply("Loading...")
  .then( async function(message) {
    // await reactInOrder(message, listEmojis, listStrs.length, listEmojisIndex);
    await reactInOrder(message, listEmojis, listStrs.length, listEmojisIndex)
          .then(async function () {
            await message.react(cancelEmoji);
          });



     await message.edit(promptMessageString); // wait until done placing reactions to show menu???


     // REACTION COLLECTOR
     const filterD = (reaction, user) => {
       // console.log("respondantId: " + respondantId);
       // //
       // console.log("filter user.id: " + user.id);
       if(user.id != respondantId) return false;

       if(reaction.emoji.name == cancelEmoji) return true;
       if(listEmojis.includes(reaction.emoji.name)) return true;

       // console.log("reaction.emoji: " + reaction.emoji);
       // // console.log("yesEmoji: " + yesEmoji);
       // console.log("reaction.emoji.name: "+ reaction.emoji.name);
       // return true;
       // if(reaction.emoji.name == yesEmoji || reaction.emoji.name == noEmoji) return true;
       return false;// filter for only the user the prompt is for and for the correct emojis
     };

     console.log("time: "+ time_ms);
     const collector = message.createReactionCollector(filterD, {"time": time_ms });


     collector.on('collect', (reaction, user) => {
       console.log("Collected!! *.*");

     });

     collector.on('end', (collected, reason) => {

       console.log("Collector ended");
       console.log("Reason: "+ reason);
     });


  });



}




//
//
//
// module.exports.chooseFromListA = function(msg, promptStr, listStrs, listEmojis, callback, timeoutCallback) {
//   // REJECT IF LESS THAN 2 CHOICESS!!!!!
//   // if asdasd
//   //load from preset list
//   if(listEmojis == "default"){
//     if (listStrs.length > chooseFromListDefault) {
//       console.log("ERR: chooseFromList listStrs.length > default emoji length");
//       return;
//     }
//     //set listEmojis
//     listEmojis = chooseFromListDefault;
//   } else if (listEmojis == "numbers") {
//     if (listStrs.length > reaction_numbers) {
//       console.log("ERR: chooseFromList listStrs.length > reaction_numbers.length");
//       return;
//     }
//     //set listEmojis
//     listEmojis = reaction_numbers;
//   }
//
//   if (listStrs.length > listEmojis.length) {
//     console.log("ERR: chosseFromList listStrs.length is < listEmojis.length  >.<");
//     return false;
//   }
//
//   var respondantId = msg.author.id;
//
//   //form prompt message with list
//
//   //generate list with emojis
//   promptListStr = "";
//   for (var i = 0; i < listStrs.length; i++) {
//     thisChoiceStr = listEmojis[i] + " - "+ listStrs[i] + "\n";
//     promptListStr += thisChoiceStr;
//   }
//
//   promptMessageString = promptStr + "\n" + promptListStr;
//
//
//   async function reactInOrder(message, reactArr, amount, index) {
//
//     // var thisReaction = dClient.emojis.cache.find(emoji => emoji.name === reactArr[index]);
//     var thisReaction = reactArr[index];
//
//     index++;
//     console.log("thisReaction:");
//     console.log(thisReaction);
//     message.react(thisReaction)
//     .then(() => {
//       if(index >= amount || index >= reactArr.length) {
//         return; //done
//       } else {
//         reactInOrder(message, reactArr, amount, index);
//
//       }
//
//     });
//   }
//
//   var listEmojisIndex = 0;
//   msg.reply(promptMessageString)
//   .then( async function(message) {
//     await reactInOrder(message, listEmojis, listStrs.length, listEmojisIndex);
//
//   });
//
//
//
//
//
//
//
// }


// module.exports = {
//   "client": null,
//   "config": null,
//   "init": function init(client, config) {
//
//     if(!client) {
//       console.log("ERR: dbot init failure, client invalid");
//       return false;
//     }
//
//     if(!config) {
//       console.log("ERR: dbot init failure, client invalid");
//       return false;
//     }
//
//     return true;
//   },
//   "dmAdmin" : function(msgToAdmin) {
//     var adminId = this.config.adminId
//
//     try {
//       client.users.fetch(adminId)
//       .then(user => {
//         user.send(msgToAdmin)
//
//       })
//
//     } catch (err) {
//       console.log("ERR: dbot dmAdmin failed >.<");
//       console.log("ERR:");
//       console.log(err);
//
//     }
//   }
//
//
//
//
//
//
//
//
// }
