//help message to PM to users

//list commands here
// module.exports.main = "\
// To run a command in Milliways, use:\n\
// `<!command>`\n\
// \n\
// __**Avaliable commands in Milliways**__\n\
// \n\
// **!ping** - Pings the bot for testing\n\
// **!d** - Rolls a dice, supports any non-zero integer to set dice sides <!d 20>\n\
// **!remind** - Add a personal reminder <!remind me to checkout reminders in 1 minute> IN BETA\n\
// \n\
// **!bug** - Sends a bug report to The Sirius Cybernetics Corporation\n\
// **!suggest** - Sends a suggestion to The Sirius Cybernetics Corporation"
//
// var mainMsg = "\
// `<!command>`\n\
// \n\
// __**Avaliable commands in Milliways**__\n\
// \n\
// **!ping** - Pings the bot for testing\n\
// **!d** - Rolls a dice, supports any non-zero integer to set dice sides <!d 20>\n\
// **!remind** - Add a personal reminder <!remind me to checkout reminders in 1 minute> IN BETA\n\
// \n\
// **!bug** - Sends a bug report to The Sirius Cybernetics Corporation\n\
// **!suggest** - Sends a suggestion to The Sirius Cybernetics Corporation";

var mainMsg = "\
To run a command use:\n\
`!<command>`\n\
\n\
__**Avaliable commands in Milliways**__\n\
\n\
Use `!help <command>` for details on a specific command\n\
\n\
__**General:**__\n\
\n\
**!ping** - Sends Marvin a ping\n\
**!stats** - Get the status of Marvin\n\
**!d** - Rolls a dice <!d 20>\n\
**!remind** - IN BETA: Add a personal reminder\n\
**!myreminds** - IN BETA: lists all of your reminds\n\
**!juke** - IN BETA: Play music from YouTube in the voice channel\n\
\n\
__**OSRS:**__\n\
\n\
**!birdhouses** - Sets a reminder for your birdhouse run\n\
**!herbrun** - Sets a reminder for your herb run\n\
\n\
__**Tools:**__\n\
\n\
**!asciito** - Converts ascii to numbers from base 2-32 and more\n\
**!base** - Converts integers from base 2-32 to another base 2-32\n\
**!split** - Splits a string of characters/numbers into chunks\n\
**!cshift** - Apply a caeser shift or bruteforce shifted ciphertext\n\
**!demorse** - Decode standard ascii morse (.-/) tos plaintext\n\
**!ioc** -  Calculate the IOC and Kappa of a given string.\n\
\n\
__**SCC:**__\n\
\n\
**!bug** - Sends a bug report to The Sirius Cybernetics Corporation\n\
**!suggest** - Sends a suggestion to The Sirius Cybernetics Corporation\
";

module.exports.mainHelpEmbed = {
  embed:{
    color: 0x88BC50,
    description: mainMsg

  }
}



// var pingMsg = "\
// \
//
// "
// module.exports.ping = {
//   embed:{
//     color: 0x88BC50,
//     description: mainMsg
//
//   }
// }
// var dMsg = "\
// Rolls a dice\n\
// Supply any non-zero integer to set the amount of sides the dice has";

// var dMsg = "\
// Rolls a dice\n\
// Supply any non-zero integer to set the amount of sides of the dice";

var pingMsg = "\
__**Command:**__ `!ping`\n\
\n\
Sends a ping to Marvin!\n\
\n\
Use to check if Marvin is online.\n\
Returns the one-way ping (heart beat) from the Discord websocket.\n\
\n\
**Example:**\n\
`!ping`\n\
";
module.exports.ping = {
  embed:{
    color: 0x88BC50,
    description: pingMsg

  }
}

var statsMsg = "\
__**Command:**__ `!stats`\n\
\n\
Get some stats on Marvin.\n\
\n\
Shows how long Marvin has been online, memory usage, and database status.\n\
\n\
**Example:**\n\
`!stats`\n\
";
module.exports.stats = {
  embed:{
    color: 0x88BC50,
    description: statsMsg

  }
}

var dMsg = "\
__**Command:**__ `!d`\n\
\n\
Rolls a dice.\n\
Supply any non-zero integer to set the amount of sides of the dice.\n\
\n\
**Format:**\n\
`!d [sides]`\n\
\n\
**Example:**\n\
`!d 20`\n\
";
module.exports.d = {
  embed:{
    color: 0x88BC50,
    description: dMsg

  }
}


var remindMsg = "\
__**Command:**__ `!remind`\n\
\n\
```diff\n\
-····IN BETA·····-\n\
!---known bugs---!\n\
\n\
!Sometime sends doubles\
```\n\
Set a personal reminder.\n\
Sends you a DM with your reminder at the time you specify.\n\
\n\
**Format:**\n\
`!remind me to [reminder text here] in [timeFromNow] [timeMeasurment]`\n\
\n\
[timeMeasurment] supports minute(s), hour(s), day(s), week(s), month(s), etc.\n\
\n\
**Example:**\n\
`!remind me to try out reminders in 10 seconds`\n\
";
module.exports.remind = {
  embed:{
    color: 0x88BC50,
    description: remindMsg

  }
}

var myremindsMsg = "\
__**Command:**__ `!myreminds`\n\
\n\
Get a list of your current reminders.\n\
\n\
**Example:**\n\
`!myreminds`\n\
";
module.exports.myreminds = {
  embed:{
    color: 0x88BC50,
    description: myremindsMsg

  }
}

var jukeMsg = "\
__**Command:**__ `!juke <command>`\n\
\n\
```diff\n\
-····IN BETA·····-\n\
!---known bugs---!\n\
\n\
!videos can take a few minutes to load\n\
!videos can cause juke to crash and playlist be lost ):\
```\n\
Play music from YouTube in the voice channel\n\
\n\
**Juke commands:**\n\
play - plays the song at the supplied url and adds it to the playlist\n\
skip - skips the currently playing song\n\
stop - stops juke all together and ends the playlist\n\
volume - set the volume or leave out the number to display the current volume\n\
list - lists the current playlist\n\
\n\
**Format:**\n\
`!juke play [YOUTUBE_VIDEO_FULL_URL_HERE]`\n\
`!juke skip`\n\
`!juke stop`\n\
`!juke volume`\n\
`!juke volume [DESIRED_VOLUME_1-10]`\n\
`!juke list`\n\
\n\
**Example:**\n\
`!juke play https://www.youtube.com/watch?v=hTXOW_jJdKE`\n\
";
module.exports.juke = {
  embed:{
    color: 0x88BC50,
    description: jukeMsg

  }
}



var birdhousesMsg = "\
__**Command:**__ `!birdhouses`\n\
\n\
Sets a reminder to harvest your birdhouses in 50 minutes.\n\
\n\
**Example:**\n\
`!birdhouses`\n\
";
module.exports.birdhouses = {
  embed:{
    color: 0x88BC50,
    description: birdhousesMsg

  }
}

var herbrunMsg = "\
__**Command:**__ `!herbun`\n\
\n\
Sets a reminder to harvest your herbs in 80 minutes.\n\
\n\
**Example:**\n\
`!herbun`\n\
";
module.exports.herbrun = {
  embed:{
    color: 0x88BC50,
    description: herbrunMsg

  }
}
// module.exports.d = {
//   embed:{
//     color: 0x88BC50,
//     description: "__**Command :**__ `!d`",
//     fields:[{
//       name: "Description",
//       value: dMsg
//     }]
//
//   }
// }

// module.exports.d = {
//   embed:{
//     color: 0x88BC50,
//     description: "__**Command :**__ `!d`",
//     fields:[{
//       name: "Description",
//       value: dMsg
//     }]
//
//   }
// }



// module.exports.d = {
//   embed:{
//     color: 0x88BC50,
//     fields:[{
//       name: "_**Command !d**_",
//       value: dMsg
//     }]
//
//   }
// }


// module.exports.d = {
//   embed:{
//     color: 0x88BC50,
//     description: dMsg
//
//   }
// }
var bugMsg = "\
__**Command:**__ `!bug`\n\
\n\
Submits a bug report.\n\
Also sends a copy to you in a DM.\n\
\n\
It is important to The Sirius Cybernetics Corporation to provide customers with an outlet for all implications and aspersions.\n\
\n\
**Format:**\n\
`!bug [type out bug report here!]`\n\
\n\
**Example:**\n\
`!bug bugss here`\n\
";
module.exports.bug = {
  embed:{
    color: 0x88BC50,
    description: bugMsg

  }
}

var suggestMsg = "\
__**Command:**__ `!suggest`\n\
\n\
Submits a suggestion.\n\
Also sends a copy to you in a DM.\n\
\n\
The Sirius Cybernetics Corporation complaints department thanks you for all of your projected criticisms!\n\
\n\
**Format:**\n\
`!suggest [type out your suggestions here!]`\n\
\n\
**Example:**\n\
`!suggest suggestions here`\n\
";
module.exports.suggest = {
  embed:{
    color: 0x88BC50,
    description: suggestMsg

  }
}

var asciitoMsg = "\
__**Command:**__ `!asciito`\n\
\n\
Marvin can convert a string of ascii to integers!\n\
\n\
The base of the output numbers can be anything from 2-32.\n\
\n\
Also supports shorthand for setting the outBase/conversion type:\n\
binary = 2\n\
bits = 2\n\
dec = 10\n\
hex = 16\n\
morse = standard ascii to morse (.-/) conversion\n\
\n\
**Format:**\n\
`!asciito [outBase] [\"text here wrapped in quotes\"]`\n\
\n\
**Examples:**\n\
`!asciito hex \"Hello world!\"`\n\
`!asciito bits \"Hello world!\"`\n\
`!asciito 21 \"Hello world!\"`\n\
`!asciito morse \"Hello world!\"`\n\
";
module.exports.asciito = {
  embed:{
    color: 0x88BC50,
    description: asciitoMsg

  }
}


var baseMsg = "\
__**Command:**__ `!base`\n\
\n\
Marvin can convert a set of integers from one base to another,\n\
or a set of numbers to ascii!\n\
\n\
The base of the supplied numbers can be base 2-32.\n\
\n\
The output type that you wish to convert to can be ascii or numbers base 2-32.\n\
\n\
Supports shorthand for setting the inBase/outBase:\n\
binary = 2\n\
bits = 2\n\
hex = 16\n\
ascii = (outBase only to convert to ascii)\n\
\n\
__WARNING!!! Inaccurate results with inputs including new lines\n\
^^^(returns only, not natural wrapping)__\n\
\n\
**Format:**\n\
`!base [inBase] [outBase] [\"numbers here separated by one space\"]`\n\
\n\
**Examples:**\n\
`!base 10 hex \"42\"`\n\
`!base 10 3 \"42\"`\n\
`!base 10 bits \"42\"`\n\
`!base bits 10 \"101010\"`\n\
`!base bits hex \"101010\"`\n\
\n\
`!base hex ascii \"4c 69 66 65 2e 20 44 6f 6e 27 74 20 74 61 6c 6b 20 74 6f 20 6d 65 20 61 62 6f 75 74 20 6c 69 66 65 2e\"`\n\
";
module.exports.base = {
  embed:{
    color: 0x88BC50,
    description: baseMsg

  }
}


var splitMsg = "\
__**Command:**__ `!split`\n\
\n\
Marvin can divide a string of characters into chunks of a size you define!\n\
\n\
Can be used for separating a string of ascci, bits, hex.\n\
Useful for formating data into space delimited list\n\
\n\
**Format:**\n\
`!split [maxChunkSize] [\"stringheretobedivided\"]`\n\
\n\
**Examples:**\n\
`!split 2 \"AaBbCcDd\"`\n\
`!split 2 \"466f7274792054776f\"`\n\
`!split 8 \"0011010000110010\"`\n\
";
module.exports.split = {
  embed:{
    color: 0x88BC50,
    description: splitMsg

  }
}

var cshiftMsg = "\
__**Command:**__ `!cshift`\n\
\n\
Caesar Shift - Apply a caeser shift of any integer to a string,\n\
or brute force it!\n\
\n\
**Format:**\n\
`!cshift [shiftValue] [\"string here to be shifted\"]`\n\
`!cshift break [\"string here to be brute forced\"]`\n\
\n\
**Examples:**\n\
`!cshift 3 \"ONE ZERO ONE ZERO ONE ZERO\"`\n\
`!cshift -13 \"TRG FUVSGL JVGU VG\"`\n\
`!cshift break \"VGG TJPM NCDAO VMZ WZGJIB OJ PN\"`\n\
";
module.exports.cshift = {
  embed:{
    color: 0x88BC50,
    description: cshiftMsg

  }
}


var demorseMsg = "\
__**Command:**__ `!demorse`\n\
\n\
Decode Morse - Decode standard ascii morse (.-/) to plaintext\n\
\n\
**Format:**\n\
`!demorse [\"morse code here to be decoded\"]`\n\
\n\
**Examples:**\n\
`!demorse \"..-. --- ..- .-. - -.-- / - .-- ---\"`\n\
`!demorse \"-. --- - / .-- .- -... ..- -. / -.-. --- -.. .\"`\n\
`!demorse \"-.-. --.- / -.-. --.- / -.-. --.-\"`\n\
";
module.exports.demorse = {
  embed:{
    color: 0x88BC50,
    description: demorseMsg

  }
}


var iocMsg = "\
__**Command:**__ `!ioc`\n\
\n\
Index of Coincidence - Calculate the IOC and Kappa of a given string.\n\
\n\
**Format:**\n\
`!ioc [\"Plaintext here to be analyzed\"]`\n\
\n\
**Examples:**\n\
`!ioc \"THIS IS A SUBSTITUTION CIPHER\"`\n\
`!ioc \"MIOL OL A LWZLMOMWMOGF EOHITK\"`\n\
`!ioc \"UIJT JT B TVCTUJUVUJPO DJQIFS\"`\n\
";
module.exports.ioc = {
  embed:{
    color: 0x88BC50,
    description: iocMsg
  }
}






// “Hardly worth anyone’s while to help a menial robot, is it ?… I mean, where’s the percentage in being kind or helpful to a robot if it doesn’t have any gratitude circuits?” –Marvin
