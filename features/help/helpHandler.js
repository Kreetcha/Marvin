var helpMsgs = require('./helpMsgs.js')



module.exports = function(msg ,cmdArgs){
  //if no args supplied send main menu
  if (typeof cmdArgs[0] === 'undefined') {
    msg.author.send({"embeds": [helpMsgs.mainHelpEmbed.embed] });
  } else {
    //check through help messages to see if there is one for that command
    var helpArg = cmdArgs[0];
    if (helpMsgs.hasOwnProperty(helpArg)) {
      msg.author.send({"embeds": [helpMsgs[helpArg].embed] });

    } else {
      //place unknown command warning and easter eggs here
      msg.author.send({"embeds": [helpMsgs.mainHelpEmbed.embed] });
    }

  }


}
