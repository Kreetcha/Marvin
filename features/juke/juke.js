'use strict'
const ytdl = require("ytdl-core");
const { joinVoiceChannel,
		createAudioPlayer,
		createAudioResource,
		StreamType,
		AudioPlayerStatus } = require('@discordjs/voice');

const dbot = require("../../features/dbot/dbot.js")

const queue = new Map();

var client = null;

const { exec } = require('child_process');

//https://stackoverflow.com/questions/69028269/error-connection-play-is-not-a-function-in-discordjs-v13
// https://discordjs.guide/voice/voice-connections.html#access
// https://discordjs.guide/voice/audio-player.html#cheat-sheet
// https://github.com/amishshah/ytdl-core-discord/issues/391
function setClient(cl) {
  client = cl;
}

function validateURL(urlStr) {
  return ytdl.validateURL(urlStr)
}


async function execute(msg, songURL, voiceChannelInfo){

	const serverQueue = queue.get(msg.guild.id);


	//check if url exists
	// catching minget errors from here https://github.com/fent/node-ytdl-core/issues/1002

	var songInfo = false;
	try {

		songInfo = await ytdl.getInfo(songURL);

	} catch (error) {

		// ytdl could not get song info :(


		var replyMsg = false;
		// handle known errors and dbAdmin with any unkown errors
		switch (error.message) {
			case "Status code: 410":
				//probably remove most of the logging for known errors after staging
				console.log("ERR: JUKE cleanly caught [Status code: 410] error trying to get ytdl song info @ " + new Date());
				console.log("songURL: ", songURL);
				console.log("err:");
				console.log(error);
				console.log(`errorMsg: [${error.message}]`);
		        replyMsg = `Video at URL \`${songURL}\` is gone and wasn't added to the queue ;(\n(returned 410)`;

				break;
			default:
				console.log("ERR: JUKE caught unknown error trying to get ytdl song info @ " + new Date());
				console.log("songURL: ", songURL);
				console.log("err:");
				console.log(error);
				console.log(`errorMsg: [${error.message}]`);
				replyMsg = `\n**ERR:** caught error unknown attempting to get song info for video \`${songURL}\`\nerrorMessage: [${error.message}]\nadmin notified`;
				dbot.dmAdmin(`ERR: juke play caught error attempting to get song info on server: [${msg.guild.name}] channel: [${msg.channel.name}]\n songURL: [${songURL}]\nerrorMessage: [${error.message}]`);
				break;


		}		
		// reply with error msg if msg is set
		if(replyMsg) msg.reply(replyMsg);

		// why skip here makes no sense maybe idk my brain is fried @.@
		// play next song if queue exists
		//if (serverQueue && serverQueue.songs.length > 1) {
		//	serverQueue.songs.shift();
		//	play(msg, serverQueue.songs[0]);
		//}
		return

	}

   const song = {
     title: songInfo.videoDetails.title,
     url: songInfo.videoDetails.video_url
   }


  // if no serverQueue exists create one
  if(!serverQueue) {
    const queueConstruct = {
      textChannel: msg.channel,
      voiceChannelInfo: voiceChannelInfo,
      connection: null,
      player: null,
      songs: [],
      volume: 4,
      playing: true, //unused??
    }

    queue.set(msg.guild.id, queueConstruct);

    queueConstruct.songs.push(song);

    try {
			// https://discordjs.guide/voice/voice-connections.html#cheat-sheet
      // var connection = await voiceChannel.join();
	  var connection = await joinVoiceChannel(queueConstruct.voiceChannelInfo)
      queueConstruct.connection = connection;

      play(msg, queueConstruct.songs[0]);
    } catch (err) {
      console.log("ERR: JUKE could not play song :( @ "+ new Date());
      console.log("err:");
      console.log(err);

      queue.delete(msg.guild.id);

      msg.reply("ERR:\n```" + err + "```");
      return;
    }
  } else {
    serverQueue.songs.push(song);
    msg.reply(`${song.title} has been added to the queue!`)
    return;
  }



}



function skip(msg){
  const serverQueue = queue.get(msg.guild.id);
  if (!msg.member.voice.channel){
    msg.reply("You need to be in a voice channel to use this command")
    return;
  }

  if (!serverQueue) {
      msg.reply("There is currently no songQueue to skip on this server >.<");
      return;
  }
  client.user.setActivity(null);
  // serverQueue.connection.dispatcher.end()

//  serverQueue.connection.player.stop()
	serverQueue.songs.shift();
    play(msg, serverQueue.songs[0]);
}



function stop(msg){
  const serverQueue = queue.get(msg.guild.id);

  if (!msg.member.voice.channel){
    msg.reply("You need to be in a voice channel to use this command")
    return;
  }
  if (!serverQueue) {
      msg.reply("There is currently no songQueue to skip on this server >.<");
      return;
  }
  client.user.setActivity(null);
  serverQueue.songs = [];

  // serverQueue.connection.dispatcher.end(); //  CAONNOT read property end of null error happens here when nothing is playing :(

 // serverQueue.connection.player.stop()  //  CANNOT read property end of null error happens here when nothing is playing :(
  serverQueue.songs.shift();
  play(msg, serverQueue.songs[0]);

}


async function play(msg, song) {
  var guild = msg.guild
  const serverQueue = queue.get(guild.id);

  if (!song) {
		// leave voice channel
    //serverQueue.voiceChannel.leave(); // old syntax


	// leave voice channel
	//serverQueue.connection.destroy(); //new v13 syntax
	if(serverQueue && serverQueue.connection) {serverQueue.connection.destroy()}; //new v13 syntax

    queue.delete(guild.id);
    client.user.setActivity(null);
    return;
  }
//https://stackoverflow.com/questions/63199238/discord-js-ytdl-error-input-stream-status-code-416


	// play teh song @.@
	try {
		//try to get stream
		var stream = await ytdl(song.url, {
	      quality:'highestaudio',
	      highWaterMark: 1 << 25,
	      type: 'opus'
	    })
	    //.on("end", () => {
	    //  serverQueue.songs.shift();
	    //  play(msg, serverQueue.songs[0])
	    //})
	    .on("error", error => {
	      console.log("ERR: stream caught error while Juke was playing a song @ " + new Date());
	      console.log("song: ", serverQueue.songs[0]);
	      console.log("err:");
	      console.log(error);
	      console.log(`errorMsg: [${error.message}]`);


	      if(error.message == "input stream: Video unavailable"){

	        //console.log(JSON.stringify(error, null, 2));
	        //SKIP SONG
	        msg.reply(`Video \`${serverQueue.songs[0].title}\` was not available skipping...   ;(`);
	        serverQueue.songs.shift();
	        console.log("Caught video unavailable error cleanly, skipping...");
	        play(msg, serverQueue.songs[0]);

	      } else {
		// UNKNOWN ERROR
	        msg.reply(`\n**ERR:** caught unknown error playing video \`${serverQueue.songs[0].title}\`\nerrorMessage: [${error.message}]\nadmin notified, skipping video....`);
	        dbot.dmAdmin(`ERR: juke play caught unknown err on server: [${msg.guild.name}] channel: [${msg.channel.name}]\nvideo: [${serverQueue.songs[0].title}] URL: [${serverQueue.songs[0].url}]\nerrorMessage: [${error.message}]`);
	        console.log("Caught unknown video error, skipping video...");
			serverQueue.songs.shift();
	        play(msg, serverQueue.songs[0]);

	      }
	    });


	} catch (error) {

		// could not get ytdl stream :(
		console.log("ERR: JUKE caught error trying to get ytdl stream @ " + new Date());
		console.log("song: ", serverQueue.songs[0]);
		console.log("err:");
		console.log(error);
		console.log(`errorMsg: [${error.message}]`);
		msg.reply(`\n**ERR:** caught error attempting to get stream for video \`${serverQueue.songs[0].title}\`\nerrorMessage: [${error.message}]\nadmin notified, skipping video....`);
        dbot.dmAdmin(`ERR: juke play caught error attempting to get stream on server: [${msg.guild.name}] channel: [${msg.channel.name}]\nvideo: [${serverQueue.songs[0].title}] URL: [${serverQueue.songs[0].url}]\nerrorMessage: [${error.message}]`);

		serverQueue.songs.shift();
        play(msg, serverQueue.songs[0]);

		return false;

	}
	// create audio player if doesnt exist
	if(serverQueue.player == null) {

		const player = await createAudioPlayer();
		player.on('error', error => {
			console.log(`ERR: audioPlayer caught error @ ${new Date}`);
			console.log(`ERROR: ${error.message} with resource ${error.resource.metadata.title}`);
			console.log(error);
			msg.channel.send(`ERROR: (audioPlayer) ${error.message} with resource ${error.resource.metadata.title}\n skipping...`);

			// play next song if there is one
			serverQueue.songs.shift();
			play(msg, serverQueue.songs[0]);
		});

		// catch end of song
		player.on(AudioPlayerStatus.Idle, () => {
			// play next song if there is one
			serverQueue.songs.shift();
			play(msg, serverQueue.songs[0]);
		});

		serverQueue.player = player;
		await serverQueue.connection.subscribe(player);


	}

	// create audio resource
	// const resource = createAudioResource(stream, { inputType: StreamType.Opus, inlineVolume: true});

	//const resource = await createAudioResource(stream, { inlineVolume: true });
	const resource = await createAudioResource(stream, { metadata:{title: song.title }, inlineVolume: true});
	//const resource = await createAudioResource(stream, { inputType: StreamType.Opus, metadata:{title: song.title }, inlineVolume: true});

	// const resource = await createAudioResource(stream, {inlineVolume: true})
	// resource.on("finish", () => {
 //   serverQueue.songs.shift();
 //   play(msg, serverQueue.songs[0])
 // })

	serverQueue.resource = resource;
	//subscribe createAudioPlayer to connection
	await serverQueue.player.play(serverQueue.resource);
	// await serverQueue.player.play(serverQueue.resource);
	// .on("finish", () => {
//   serverQueue.songs.shift();
//   play(msg, serverQueue.songs[0])
// })

	// const subscription = serverQueue.connection.subscribe()
  // const dispatcher = serverQueue.connection
  //   .play(ytdl(song.url, {
  //     quality:'highestaudio',
  //     highWaterMark: 1 << 25,
  //     type: 'opus'
  //   }))
  //   .on("finish", () => {
  //     serverQueue.songs.shift();
  //     play(msg, serverQueue.songs[0])
  //   })
  //   .on("error", error => {
  //     console.log("ERR: error while Juke was playing a song @ " + new Date());
  //     console.log("song: ", serverQueue.songs[0]);
  //     console.log("err:");
  //     console.log(error);
  //     console.log(`errorMsg: [${error.message}]`);
	//
	//
  //     if(error.message == "input stream: Video unavailable"){
	//
  //       //console.log(JSON.stringify(error, null, 2));
  //       //SKIP SONG
  //       msg.reply(`Video \`${serverQueue.songs[0].title}\` was not available skipping...   ;(`);
  //       serverQueue.songs.shift();
  //       console.log("Caught video unavailable error cleanly, skipping...");
  //       play(msg, serverQueue.songs[0]);
	//
  //     } else {
	// // UNKNOWN ERROR
  //       msg.reply(`\n**ERR:** caught unknown error playing video \`${serverQueue.songs[0].title}\`\nerrorMessage: [${error.message}]\nadmin notified, skipping video....`);
  //       dbot.dmAdmin(`ERR: juke play caught unknown err on server: [${msg.guild.name}] channel: [${msg.channel.name}]\nvideo: [${serverQueue.songs[0].title}] URL: [${serverQueue.songs[0].url}]\nerrorMessage: [${error.message}]`);
	// serverQueue.songs.shift();
  //       console.log("Caught unknown video error, skipping video...");
  //       play(msg, serverQueue.songs[0]);
	//
	//
  //     }
  //   });
    // dispatcher.setVolumeLogarithmic(serverQueue.volume / 11);
  serverQueue.resource.volume.setVolumeLogarithmic(serverQueue.volume / 11);
  serverQueue.textChannel.send(`Playing **${song.title}**`);
  client.user.setActivity(String.fromCharCode(9834) + " " + song.title);

}



function list(msg){
  const serverQueue = queue.get(msg.guild.id);

  if (!serverQueue) {
    msg.reply("There is currently no songQueue playing on this server >.<");
    return false;
  }

  var songNames  = []

  for (var i = 0; i < serverQueue.songs.length; i++) {
    songNames.push(serverQueue.songs[i].title);
  }
  return songNames;
}




function volume(msg, vol) {
  if (!msg.member.voice.channel){
    msg.reply("You need to be in a voice channel to use this command >.<")
    return;
  }

  const serverQueue = queue.get(msg.guild.id);
  if (!serverQueue) {
    msg.reply("There is currently no songQueue playing this server >.<");
    return false;
  }

  // const dispatcher = serverQueue.connection.dispatcher;
  // if (!dispatcher) {
  //   msg.reply("There is currently no music playing.");
  //   return false;
  // }
	//const resource = serverQueue.connection.resource;
	const resource = serverQueue.resource;
	if (!resource) {
		msg.reply("There is currently no music playing. (!resource)");
		return false;
	}


  if (vol === undefined) {
    //no volume was supplied, return volume
    // msg.reply("\nThe current volume is: `" + serverQueue.volume + "/10`");
    return serverQueue.volume;
  } else {
    //new volume was supplied


    //check if correct range
    if ( isNaN(vol) || vol > 11 || vol < 0)
    {
      console.log("ERR: juke volume command vol parsing jailure @ "+ new Date());
      return false;
    }
    //set new volume

    try {
      serverQueue.volume = vol;
      resource.volume.setVolumeLogarithmic(serverQueue.volume / 11);

      // msg.reply("\nVolume Set to `"+ vol + "/10`");
      return serverQueue.volume;
    } catch (err) {
      dbot.dmAdmin("ERR: juke setting volume failed >.< @ "+ new Date());
      console.log("ERR: juke setting volume failed >.< @ "+ new Date());
      console.log("err:");
      console.log(err);
      return false;
    }
    // msg.reply("\nERR: juke setting volume failed >.< @ "+ new Date());

  }




}



function remove(msg, trackIndex) {

  if (!msg.member.voice.channel){
    msg.reply("You need to be in a voice channel to use this command")
    return;
  }


  const serverQueue = queue.get(msg.guild.id);

  if (!serverQueue) {
    msg.reply("There is currently no songQueue playing on this server >.<");
    return false;
  };


  if (trackIndex === 0){
    msg.reply("\nCannot remove the currently playing track.\n Please use `!juke skip` to skip this track")
  }

  // var listLength

  /*
  !!! what if i remove a song at an index when a song finishes?
  this could mean removing the wrong track

  need to have a user prompt for are you sure about removing TRACK_NAME
  then on poll callback verify once more that the name of the index is the same

  NO CLUE if this will work

  may hav to add a serverQueue bool to show if list is being edited and act accordingly
  for example, dont pley next track while playlist is being edited, set timoute to try again in a few momemts

  */

  if (trackIndex >= serverQueue.songs.length) {
    msg.reply("Please supply a track index that is in the current playlist");
    return false;
  }

  var thisSong = serverQueue.songs[trackIndex];


  if (!thisSong) {
    msg.reply("ERR: juke.js remove >.<, (!thisSong)");
    return;
  }


  // msg.reply("This song: \n"+ JSON.stringify(thisSong, null, 2));


  async function removeTrackSafe(trackIndex, thisSong) { // should be safer >.<

    if (serverQueue.songs[trackIndex].title === thisSong.title) {
      serverQueue.songs.splice(trackIndex, 1);
    }
  }



  removePromptString = `Do you want to remove track ${trackIndex}: ${thisSong.title} ?`;
  dbot.areYouSure(msg, removePromptString, 10000,
    (result) => {
      if (result) {
        // msg.reply("I would remove it if i could >.<");


        try {
            // serverQueue.songs.filter( (obj, index) =>  {
            //   console.log("index: "+ index);
            //   console.log("obj: "+ obj);
            //
            //   return obj.title === thisSong.title;
            //  }); //  && index === trackIndex

            removeTrackSafe(trackIndex, thisSong).then(() => {
              // msg.reply("Track removed.");

              var songList = list(msg)
              var strOut = "";


              strOut += "Track removed.\n`New Queue:`\n";
              strOut += "```\n"
              for (var i = 0; i < songList.length; i++) {
                if (i == 0) { //QUICK SLOPPY ADD NEEDS REWRITING POF
                  var thisSongStr = String.fromCharCode(9834)+ " - "+ songList[i] + "\n"; // 9834 = music note
                } else
                {
                  var thisSongStr = i + " - "+ songList[i] + "\n";
                }
                strOut += thisSongStr;
              }
              strOut += "```";

              msg.reply(strOut)

            });


            return;
        } catch (e) {
          msg.reply("Err removing track from serverQueue >.<");
          console.log(e);
          return;
        }

      } else {
        // do nothing
      }

    },
    (reason, promptMessage) => {

        var oldMsg = promptMessage.content;
        var newMsg = oldMsg + "\n**`Menu expired`**";
        promptMessage.edit(newMsg);

    });

}


function getLinkType(urlStr) {
	// VERY LAZY CHECK FOR PURE VIDEO PLAYLIST URLS
	// this could be able to correctly identify a url type but needs further work
	if (urlStr.includes("/playlist?list=")) {
		// URL is a pure playlist link
		return {'playlist': true, "video":false};
	} else if (urlStr.includes("/watch") && urlStr.includes("list=")) {
		// URL is link to a video in a playlist
		// !!!!!!!! THIS WILL RETURN 429 TOO MANY REQUESTS
		//				IF ATTEMPTING TO getPlaytlistVideoIDs with this type
		return {'playlist': true, "video":true};
	} else if (urlStr.includes("/watch") && !(urlStr.includes("list="))) {
		// URL is a pure video link
		return {'playlist': true, "video":false};
	} else {
		// unknown url :(
		return false;

	}
}

async function getPlaylistVideoIDs(playlistURL) {
	return new Promise(function (resolve, reject) {
		//shellCmdPrefix = "youtube-dl -j --flat-playlist ";
		var shellCmdPrefix = "youtube-dl -j --flat-playlist --dump-single-json ";
		var shellCmd = shellCmdPrefix + playlistURL;

		exec(shellCmd, (error, stdout, stderr) => {
			if(error) {
				return reject(error)
			}

			resolve(stdout);

		});


	});

}


async function playPlaylist(msg, playlistURL, voiceChannelInfo) {

var maxSongsInPlaylist = 50;

var splitOptions = {
	"maxLength": 1800,
	"prepend": "`JUKE PLAYLIST OUTPUT CONTINUED:`\n```js\n",
	"append": "```",
	"char": "\n"
};
var errSplitOptions = {
	"maxLength": 1800,
	"prepend": "`JUKE PLAYLIST ERROR CONTINUED:`\n```js\n",
	"append": "```",
	"char": "\n"
}

	//attempt to use youtube-dl to get playlist ids
	getPlaylistVideoIDs(playlistURL).then( result => {

		var playlistDoc = {
			info: false,
			songs: [],
			songCount: 0
		};
		var resultLines = result.split('\n');

		for (var line of resultLines) {
			try {
				var thisLineObj = JSON.parse(line)
				console.log(JSON.stringify(thisLineObj, null, 2))
				if ('webpage_url_basename' in thisLineObj && thisLineObj.webpage_url_basename == 'playlist') {
					// this is the playlist info doc

					//check if this is a youtube playlist
					if('extractor_key' in thisLineObj && thisLineObj.extractor_key != 'YoutubeTab') {
						msg.reply("Only YouTube Playlists are supported >.<")
						return false;
					}
					// this obj is the playlist information, save it to playlistDoc
					playlistDoc.info = thisLineObj

				} else if ('ie_key' in thisLineObj && thisLineObj.ie_key == 'Youtube'){
					// this doc is a youtube video in the playlist, save to playlist doc
					playlistDoc.songs.push(thisLineObj)
					playlistDoc.songCount++;

					if(playlistDoc.songCount > maxSongsInPlaylist ) {
						msg.reply("Only playlists with 50 videos or less are supported >.<")
						return false
					}

				}
			} catch(e) {


			}
		}


		var outStr = `Playlist Title: ${playlistDoc.info.title}\n\`\`\`js\n`

		var songIndex = 0;
		for (var song of playlistDoc.songs) {
			outStr += `${songIndex}) [${song.id}] ${song.title}\n`
			songIndex++;
		}
		outStr += '\n```'
		//dbot.replyMessageSplit(msg, splitOptions, `\`\`\`js\n${result}\`\`\``, );
		dbot.replyMessageSplit(msg, splitOptions, `${outStr}` );




		var playlistPromptString = `Is this the playlist you want to play? Title: ${playlistDoc.info.title} Length: ${playlistDoc.songCount} `;
		dbot.areYouSure(msg, playlistPromptString, 10000,
		async function (result) {
			if(!result) return;

			//add each song to the queue

			for (var song of playlistDoc.songs) {
				await execute(msg, song.id, voiceChannelInfo)
			}

		},
		(reason, promptMessage) => {

			var oldMsg = promptMessage.content;
			var newMsg = oldMsg + "\n**`Menu expired`**";
			promptMessage.edit(newMsg);

		});


	}).catch (error => {
		console.log(`ERR: juke playPlaylist caught error from getPlaylistVideoIDs @ ${new Date()}`);
		console.log(error);
		dbot.replyMessageSplit(msg, error, `\`JUKE PLAYLIST ERROR:\`\n\`\`\`js\n${error}\`\`\``, );
		msg.reply('ERR: caught error fetching video URLs >.<');
		
	});


	return;
}


module.exports.getLinkType = getLinkType;
module.exports.playPlaylist = playPlaylist;

module.exports.list = list;
module.exports.setClient = setClient;


module.exports.volume = volume;
module.exports.validateURL = validateURL;
module.exports.execute = execute;
module.exports.skip = skip;
module.exports.stop = stop;
module.exports.play = play;
module.exports.remove = remove;
