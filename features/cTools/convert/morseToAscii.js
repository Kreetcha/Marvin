const morseToAsciiLookup = {
".-": "A",
"-...": "B",
"-.-.": "C",
"-..": "D",
".": "E",
"..-.": "F",
"--.": "G",
"....": "H",
"..": "I",
".---": "J",
"-.-": "K",
".-..": "L",
"--": "M",
"-.": "N",
"---": "O",
".--.": "P",
"--.-": "Q",
".-.": "R",
"...": "S",
"-": "T",
"..-": "U",
"...-": "V",
".--": "W",
"-..-": "X",
"-.--": "Y",
"--..": "Z",
"-----": "0",
".----": "1",
"..---": "2",
"...--": "3",
"....-": "4",
".....": "5",
"-....": "6",
"--...": "7",
"---..": "8",
"----.": "9",
"/": " ",
".-.-.-": ".",
"--..--": ",",
"---...": ":",
"..--..": "?",
".----.": "\'",
"-....-": "-",
"-..-.": "/",
".-..-.": "\"",
".--.-.": "@",
"-...-": "="
}

//brackes/parentheses = -.--.- how to implement this?!?!
//need to add special characers like Ä, Ñ etc


//https://morsecode.scphillips.com/morse2.html


module.exports = function (stringIn) {
  var stringOut;
  var convOutArray = [];
  var inArray;
  var lastChunk = "null";
  if (typeof stringIn === 'undefined') return 'ERROR: morseToAscii requires an input string';
  if (typeof stringIn !== 'string') return 'ERROR: morseToAscii requires a string as an input';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes:'+stringIn;
  }

    var convInArray = stringIn.substring(1, stringIn.length-1).split(" ");

    for (var i = 0; i < convInArray.length; i++) {
      var currentChunk = convInArray[i]
      //if last char was a space then this char was intented to be a SPACE
      // if NOT this space was intended to end a letter
      if ( currentChunk === " " && currentChunk === lastChunk) {
        convOutArray.push(" ");

      } else {
        if (morseToAsciiLookup.hasOwnProperty(currentChunk)) {
          convOutArray.push(morseToAsciiLookup[currentChunk]);
        } else {
          convOutArray.push(currentChunk);
        }
        lastChunk = currentChunk;


      }
      //convOutArray.push(String.fromCharCode(parseInt(convInArray[i],base)));
    }

  stringOut = convOutArray.join("");

  return stringOut;

};

//function getKeyByValue(object, value) { return Object.keys(object).find(key => object[key] === value); }
