module.exports = function (inBase, outBase, stringIn) {
  //console.log("inBase: " + inBase);
  //console.log("outBase: " + outBase);
  //console.log("stringIn: (" + stringIn +')');

  if(typeof inBase === 'undefined') {
    return 'ERROR: baseToBase requires (inBase, outBase, stringIn)';
  } else if (isNaN(inBase)) {
    return 'ERROR: inBase NaN';
  } else if(parseInt(inBase) > 32 || parseInt(inBase) < 2) {
    return 'ERROR: inBase requires a integer from 2-32';
  } else {
    inBase = parseInt(inBase);
  }

  if(typeof outBase === 'undefined') {
    return 'ERROR: baseToBase requires (inBase, outBase, stringIn)';
  } else if (isNaN(outBase)) {
    return 'ERROR: outBase NaN';
  } else if (parseInt(outBase) > 32 || parseInt(outBase) < 2) {
    return 'ERROR: outBase requires an integer from 2-32';
  } else {
    outBase = parseInt(outBase);
  }

  if(typeof stringIn === 'undefined') {
    return 'ERROR: baseToBase requires (inBase, outBase, stringIn)'
  } else if (typeof stringIn !== 'string') {
    return 'ERROR: stringIn requires a string wrapped in quotes';
  } else if (typeof stringIn === 'string') {
    if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
      return 'ERROR: stringIn requires a string wrapped in quotes:'+stringIn;
    } else {
      //remove quotes

      var convInArray = stringIn.substring(1, stringIn.length-1).split(" ");
      var convOutArray = [];
      for (var i = 0; i < convInArray.length; i++) {
          var currentNumb = parseInt(convInArray[i], inBase).toString(outBase);
        if (outBase === 2) {
          var currentNumb = "00000000".substr(currentNumb.length)+ currentNumb
        }

        if (convInArray[i] === "\r") {
          currentNumb === " ";
        }

        convOutArray.push(currentNumb);
      }

      return convOutArray.join(" ");

    }
  }
  //get string from quoates


}
