module.exports = function(segmentSize, stringIn){
  var stringOut;



  if (typeof segmentSize === undefined) {
      return 'ERROR: splitString requires a segment size integer as the first argument'
  }

  if (isNaN(segmentSize)) {
    return 'ERROR: NaN splitString requires a segment size integer as a first argument'
  }
  if (!stringIn) return 'ERROR: splitString requires an input string as a second argument';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: splitString stringIn requres a string wrapped in quotes:'+stringIn;
  } else {
    stringIn = stringIn.substring(1, stringIn.length-1);
  }


  if (typeof stringIn === 'string') {
  var convArray = stringIn.match(new RegExp('.{1,'+segmentSize+'}', 'g'));
  //str.match(/(.|[\r\n]){1,n}/g);
  stringOut = convArray.join(" ");

  } else {
    return 'ERROR: unsupported data type for splitString';
  }

  return stringOut;
}
