module.exports = function (base, stringIn) {
  var stringOut;
  var convOutArray = [];
  var inArray;
  if (typeof stringIn === 'undefined') return 'ERROR: baseToAscii requires an input string';
  if (typeof stringIn !== 'string') return 'ERROR: baseToAscii requires a string as an input';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes:'+stringIn;
  }
  if (typeof base === undefined) return 'ERROR: baseToAscii requires an integer argument from 2-32';
  if (isNaN(base)) return 'ERROR: NaN asciiToBase requires an integer as a second argument to set base';
  if (base > 32 || base < 2) return 'ERROR: asciiToBase requires an integer argument from 2-32';

    var convInArray = stringIn.substring(1, stringIn.length-1).split(" ");

    for (var i = 0; i < convInArray.length; i++) {
      // if (array[i] > 256) {
      //
      // }
      convOutArray.push(String.fromCharCode(parseInt(convInArray[i],base)));
    }

  stringOut = convOutArray.join("");

  return stringOut;

};
