module.exports = function (baseOut, stringIn) {
  var stringOut;
  var convOutArray = [];
  //var inArray;
  if (typeof stringIn === 'undefined') return 'ERROR: asciiToBase requires an input string';
  if (typeof stringIn !== 'string') return 'ERROR: asciiToBase requires a string as an input';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes '+stringIn;
  }
  if (typeof baseOut === undefined) return 'ERROR: asciiToBase requires an integer argument from 2-32';
  if (isNaN(baseOut)) return 'ERROR: NaN asciiToBase requires an integer as a second argument to set base';
  if (baseOut > 32 || baseOut < 2) return 'ERROR: asciiToBase requires an integer argument from 2-32';

    var convIn = stringIn.substring(1, stringIn.length-1);

    for (var i = 0; i < convIn.length; i++) {
      // if (array[i] > 256) {
      //
      // }
      var currentNumb = convIn.charCodeAt(i).toString(baseOut)
      if (baseOut === 2) {
        currentNumb = "00000000".substr(currentNumb.length)+ currentNumb
      }
      convOutArray.push(currentNumb);
    }

  stringOut = convOutArray.join(" ");

  return stringOut;

};
