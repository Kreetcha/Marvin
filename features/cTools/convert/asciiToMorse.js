const asciiToMorseLookup = {
  "A": ".-",
  "B": "-...",
  "C": "-.-.",
  "D": "-..",
  "E": ".",
  "F": "..-.",
  "G": "--.",
  "H": "....",
  "I": "..",
  "J": ".---",
  "K": "-.-",
  "L": ".-..",
  "M": "--",
  "N": "-.",
  "O": "---",
  "P": ".--.",
  "Q": "--.-",
  "R": ".-.",
  "S": "...",
  "T": "-",
  "U": "..-",
  "V": "...-",
  "W": ".--",
  "X": "-..-",
  "Y": "-.--",
  "Z": "--..",
  "0": "-----",
  "1": ".----",
  "2": "..---",
  "3": "...--",
  "4": "....-",
  "5": ".....",
  "6": "-....",
  "7": "--...",
  "8": "---..",
  "9": "----.",
  "\ ": "/",
  ".": ".-.-.-",
  ",": "--..--",
  ":": "---...",
  "?": "..--..",
  "\'": ".----.",
  "-": "-....-",
  "/": "-..-.",
  "\"": ".-..-.",
  "@": ".--.-.",
  "=": "-...-"
}

module.exports = function (stringIn) {

  var stringOut;
  var convArray = [];
  var unsupportedChararray = [];
  if (typeof stringIn === 'undefined') return 'ERROR: asciiToMorse requires an input string';
  if (typeof stringIn !== 'string') return 'ERROR: asciiToMorse requires a string as an input';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes:'+stringIn;
  }

  stringIn = stringIn.substring(1, stringIn.length-1).toUpperCase();


  for (var i = 0; i < stringIn.length; i++) {
    currentChar = stringIn.charAt(i)
    if (asciiToMorseLookup.hasOwnProperty(currentChar)) {
      convArray.push(asciiToMorseLookup[currentChar])
    } else {
      unsupportedChararray.push(currentChar);
    }
  }

  stringOut = convArray.join(" ");
  unsupportedChars = unsupportedChararray.join("|")

  return {"stringOut":stringOut, "unsupportedChars":unsupportedChars }
}
