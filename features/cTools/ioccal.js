cTools = require('./cTools.js')

module.exports = function(stringIn){

  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes';
  }
  var stringIn = stringIn.substring(1, stringIn.length-1).toUpperCase();

  var stringStats = {
    TOTAL:0,
    A:0,
    B:0,
    C:0,
    D:0,
    E:0,
    F:0,
    G:0,
    H:0,
    I:0,
    J:0,
    K:0,
    L:0,
    M:0,
    N:0,
    O:0,
    P:0,
    Q:0,
    R:0,
    S:0,
    T:0,
    U:0,
    V:0,
    W:0,
    X:0,
    Y:0,
    Z:0
  }

  var probabiltyStats = {
    TOTAL:0,
    A:0,
    B:0,
    C:0,
    D:0,
    E:0,
    F:0,
    G:0,
    H:0,
    I:0,
    J:0,
    K:0,
    L:0,
    M:0,
    N:0,
    O:0,
    P:0,
    Q:0,
    R:0,
    S:0,
    T:0,
    U:0,
    V:0,
    W:0,
    X:0,
    Y:0,
    Z:0
  }

  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
// get string stats
  for (var i = 0; i < stringIn.length; i++) {
    currentChar = stringIn.charAt(i);
    if (stringStats.hasOwnProperty(currentChar)) {
      stringStats[currentChar]++;
      stringStats['TOTAL']++;
    }
  }

  //calculate index of coincedence
  totalAlphabet = stringStats['TOTAL'];
  for (var i = 0; i < alphabet.length; i++) {
    var currLetter = alphabet.charAt(i)
      if (stringStats[currLetter] >= 1) {
        chanceDouble = (stringStats[currLetter]/totalAlphabet)*(((stringStats[currLetter]-1)/(totalAlphabet-1)))
        probabiltyStats[currLetter] = chanceDouble;
      }

  }
  var iocVal = 0;
  for (var i = 0; i < alphabet.length; i++) {
    var currLetter = alphabet.charAt(i)
    iocVal +=probabiltyStats[currLetter];
  }

  // return JSON.stringify({probabiltyStats, "iocVal":iocVal});
  return "IOC = "+iocVal.toFixed(4)+"\nKappa = "+(iocVal*26).toFixed(4)
}
