module.exports = function (shift , stringIn) {
  var stringOut = "";
  var rot;
  var convArray;
  if (typeof stringIn === 'undefined') return 'ERROR: caesarShift requires an input string';
  if (typeof stringIn !== 'string') return 'ERROR: caesarShift requires a string as an input';
  if (stringIn.charAt(0) !== '\"' || stringIn.charAt(stringIn.length-1) !== '\"') {
    return 'ERROR: stringIn requres a string wrapped in quotes'+stringIn;
  }
  if (typeof shift === undefined) return 'ERROR: caesarShift requires an integer > 1 ';
  if (isNaN(shift)) return 'ERROR: NaN caesarShift requires an integer as the first argument to set rotation/shift count';

  //remove outer quotes
  // later add support to keep capitolization durring rotation!!
  var convIn = stringIn.substring(1, stringIn.length-1).toUpperCase();
  rot = parseInt(shift)%26;


  for (var i = 0; i < convIn.length; i++) {
    //convert the character to a number
    var currentChar = convIn.charCodeAt(i)

    //if char is not in alphabet dont rotate!!
    if (currentChar < 65 || currentChar > 90) {
      stringOut = stringOut+String.fromCharCode(currentChar);
      continue;
    }
    //apply rotation
    var currentNumb = currentChar+rot;
    //now apply wrapping
    if (currentNumb < 65) {
      currentNumb = 91-(65-currentNumb);
    } else if (currentNumb > 90) {
      currentNumb = 64+(currentNumb-90)
    }

    //convert back to ascii
    stringOut = stringOut+String.fromCharCode(currentNumb);
  }

  return stringOut;

}



//all non alphabetic characters will not be shifted
