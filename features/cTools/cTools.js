module.exports.baseToBase = require('./convert/baseToBase.js');
module.exports.baseToAscii = require('./convert/baseToAscii.js');
module.exports.splitString = require('./convert/splitString.js');
module.exports.asciiToBase = require('./convert/asciiToBase.js');
module.exports.caesarShift = require('./caesarShift.js');
module.exports.morseToAscii = require('./convert/morseToAscii.js');
module.exports.asciiToMorse = require('./convert/asciiToMorse.js');
module.exports.ioccal = require('./ioccal.js');
