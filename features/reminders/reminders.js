

function reminderCmdHandler(db, msg, cmdArgs){
  let argsLength = cmdArgs.length;
  if (argsLength < 6) {
    //tell user what happened wrong
    //check for !remind list here?
    // msg.reply('PLACEHOLDER FOR REMIND COMMAND EXAMPLE (you did it wrong!)');
    msg.reply("User error: invalid format for setting reminder\n\nUse `!help remind` to learn about how to use reminders")
  } else {
    // let recipient = cmdArgs[0];
    // //check if recepient exists on server here
    let recipientName = msg.author.username;
    let recipientId = msg.author.id;
    console.log(cmdArgs);
    let timeMeasurment = cmdArgs.pop();
    let timeValue = cmdArgs.pop();
    // second preposition here NEED TO ADD CHECKING FOR THIS
    //pop off second preposition
    cmdArgs.pop();
    //shift out recipient username
    cmdArgs.shift();
    console.log(cmdArgs);
    let preposition = cmdArgs.shift().toLowerCase();
    console.log(preposition);
    console.log(cmdArgs);
    let reminderTxt = cmdArgs.join(" ");
    console.log(cmdArgs);
    // msg.reply('reminder text:\n'+reminderTxt) //moving reminderSet response until after input sanitation
    if (preposition === 'to') {
      if (isNaN(timeValue)) {
        //tell user how to enter number value for time
        msg.reply('\'to\'Please Enter a number value for time');

        return;
      }
        if (timeMeasurment === 'seconds') {
          var timeOfReminder = Date.now()+(timeValue*1000);
        } else if (timeMeasurment === 'minute' || timeMeasurment === 'minutes') {
          var timeOfReminder = Date.now()+(timeValue*60000);
        } else if (timeMeasurment === 'hour' || timeMeasurment === 'hours') {
          var timeOfReminder = Date.now()+(timeValue*3600000);
        } else if (timeMeasurment === 'day' || timeMeasurment === 'days') {
          var timeOfReminder = Date.now()+(timeValue*86400000);
        } else if (timeMeasurment === 'week' || timeMeasurment === 'weeks') {
          var timeOfReminder = Date.now()+(timeValue*604800000);
        } else if (timeMeasurment === 'month' || timeMeasurment === 'months') {
          var timeOfReminder = Date.now()+(timeValue*2592000000);
        } else if (timeMeasurment === 'rel' || timeMeasurment === 'rels') {
          var timeOfReminder = Date.now()+(timeValue*1200);
        } else {
          //tell user about unsupported measurment of time.
          msg.reply('unsupported measurment of time')
          //support other sci-fi measurments of time here?
          return;
        }

      } else {
        //inform user that 'to' is currently the only supported preposition
        msg.reply('\'to\' is currently the only supported preposition');

        return;
      }
      //create reminder doc and attempt to save reminder to db
      var reminderDoc = {}
      reminderDoc.recipientName = recipientName;
      reminderDoc.recipientId = recipientId
      reminderDoc.reminderTxt = reminderTxt;
      reminderDoc.timeToRemind = timeOfReminder;
      var saveTry = saveReminder(db, reminderDoc);
      if (saveTry.ok == false) {
        msg.reply("There was an error saving your reminder to db!\nPlease try again and PLEASE contact admin if issue persists.")
      }
      var setReminderStr = `Reminding you to:\n\`${reminderTxt}\`\n@ ${new Date(timeOfReminder)}`
      msg.reply(setReminderStr);
      return;
    }

    //handling I'm and your etc could be added later?

    console.log('failed reminder creation');
    //msg.reply('testing remind asdasd');
    //is return here unessesary? :P
    return;
  };

function saveReminder(db, doc){
if (typeof db == "undefined") {
  return {"ok":false, "err":"db undefined when saving reminder @ "+ new Date()}
}
db.collection('reminders').insertOne(doc, function(err, res){
  if (err) {
    console.log("ERROR: reminderSave failed @ "+new Date());
    console.log(err);
    return {"ok":false, "err":err}
  }
  console.log("reminder saved");
});
  return {"ok":true}
}

function createReminder(db, recipientName, recipientId, reminderText, timeOfReminder){
  var reminderDoc = {}
  reminderDoc.recipientName = recipientName;
  reminderDoc.recipientId = recipientId
  reminderDoc.reminderTxt = reminderText;
  reminderDoc.timeToRemind = timeOfReminder;
  // var newReminder = Reminders(reminderDoc).save(function(err, data){
  //   if(err) throw err;
  // });
  return saveReminder(db, reminderDoc)
}

function checkAndIssueReminders(db, client){
  if (typeof db == "undefined") {
    console.log("db not connected!!")
    console.log("ERROR: error retrieving reminders @ "+new Date());
    return
  }
  var nowTime = Date.now()
  // db.collection('reminders').find({}).toArray ((err,result) => {
  db.collection('reminders').find({"timeToRemind":{$lt:nowTime}}).toArray ((err,result) => {

    if (err) {
      console.log("ERROR: error retrieving reminders @ "+new Date());
      console.log(err);
      return
    }
    // console.log(result);
    for (reminder of result){
      // console.log(reminder);
      // console.log(reminder._id);
       db.collection('reminders').findOneAndDelete({"_id":reminder._id}, (err, result) =>{
         if (err) {
           console.log("ERROR: error findOneAndDelte while over reminder docs to issue with cursor @ "+new Date());

         }
         var reminderToIssue = result.value;
         console.log('Attempting to issue reminder: ', reminderToIssue);
         var reminderOut = "Reminding you to `"+reminderToIssue.reminderTxt+"`";
         // var reminderUser = client.users.fetch(reminderToIssue.recipientId);
         try {
           client.users.fetch(reminderToIssue.recipientId)
           .then(user => {
             user.send(reminderOut)

           })

         } catch (err) {
           console.log("ERR: REMINDER issue failed >.< @ "+ new Date());
           console.log("ERR:");
           console.log(err);

           //DM ADMIN ABOUT THIS ERROR
           try {
             client.users.fetch(config.adminId)
             .then(user => {
               user.send("ERR: REMINDER issue failed >.< @ "+ new Date())

             })

           } catch (err) {
             console.log("ERR: dm to admin regarding reminder issue failure has failed >.<");
             console.log("ERR:");
             console.log(err);

           }

         }
         //reminderUser.send(reminderOut);

         // console.log("find and delete test");
         // console.log(result.value);
       })
      // db.collection('reminders').deleteOne({"_id":reminder._id})
      //   .then((asd)=>{console.log(asd)})
    }
  });
}
module.exports.createReminder = createReminder
module.exports.handler = reminderCmdHandler;
module.exports.checkAndIssueReminders = checkAndIssueReminders;
