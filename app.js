//const Discord = require('discord.js');
//const client = new Discord.Client();

const { Client, Intents } = require('discord.js');
const client = new Client({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_BANS,
		Intents.FLAGS.GUILD_INVITES,
		Intents.FLAGS.GUILD_VOICE_STATES,
		Intents.FLAGS.GUILD_MESSAGES,
		Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
		Intents.FLAGS.GUILD_MESSAGE_TYPING,
		Intents.FLAGS.DIRECT_MESSAGES,
		Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
		Intents.FLAGS.DIRECT_MESSAGE_TYPING
	]
});
var cron = require('node-cron');

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require("mongodb").ObjectID;


var Chance = require('chance'),
    chance = new Chance();

const config = require('./config.json');

var cmdPrefix = config.cmdPrefix
// const atlasUri = config.mongoAtlasUrl;
//bot features
const reminders = require('./features/reminders/reminders.js');
const helpHandler = require('./features/help/helpHandler.js');
const commandHandler = require('./cmd/commandHandler.js');
const redditAlert = require('./features/redditAlert/redditAlert.js');
const rollCall = require('./features/rollCall/rollCall.js');

const juke = require('./features/juke/juke.js');

const dbot = require('./features/dbot/dbot.js');


const token = config.token;

//MONGODB ATLAS
var db;
MongoClient.connect(config.mongoAtlasUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (error, mClient) => {
  if (error) {
    // console.log();
    // console.error(err);
    throw error
  }
  //dbString is to allow different mongo databases for staging bots/etc
  var dbString = config.mongoDbName;
  // db = mClient.db("marvinDb")
  db = mClient.db(dbString)
  console.log("Connected to `" + dbString + "`! @ "+ new Date());

  // reminderColl = db.collection("reminders")

  //start reminder polling
  setTimeout(function(){
    reminders.checkAndIssueReminders(db, client)
    setTimeout(arguments.callee, 2000);
  }, 2000)

  //schuedule redditAlert polling
  cron.schedule('*/15 * * * *', () =>{
    redditAlert.fgosScrape(db, client, config)
  });

  // //schedule rollCall
  // if (config.rollCallIDs.length > 0) {
  //   cron.schedule('*/10 * * * *', () =>{
  //     rollCall.doRollCall(config, client);
  //   });
  // }
});



//GLOBAL VARS
// var dsMainChannel;

//mongo connect
var lastDate;
var reminderList;

///////////////////////////////////////////////////////////////////////////////////////
//Discord login /start
console.log('Attempting to login to discord');
client.on('ready', () => {
  console.log(`Logged in to discord as ${client.user.tag} ! ^.^ @ ${new Date()}`);

  //init dbot lib
  if(!dbot.init(client, config)) {
    console.log("ERR: dbit init FAILED >.< @ "+ new Date());
    process.exit(); // this is dirty >.<
  }
  juke.setClient(client);
  if (config.loginAlert == true) {
    try {
      client.users.fetch(config.adminId)
      .then(user => {
        user.send("I just logged on to discord @ "+ new Date())

      })

    } catch (err) {
      console.log("ERR: loginAlert failed >.<");
      console.log("ERR:");
      console.log(err);

    }
    //client.users.get(config.adminId).send("I just logged on to discord @ "+ new Date())
  }
  // client.users.get(config.adminId).send("I just logged on to discord @ "+ new Date())
  //alert admin of startup

  //find and store channel objects
  // dsMainChannel = client.channels.find('name', 'general');
  // dsMainChannel.send('*sigh*');
  //poll for new reminders to issue
  // setTimeout(function(){
  //   reminders.checkAndIssueReminders(db, client)
  //   setTimeout(arguments.callee, 2000);
  // }, 2000)
  // setTimeout(function(){
  //   reminders.reminderWatch(client);
  //   //dsMainChannel.send('1 second test '+(Date.now()-lastDate));
  //   // console.log('REMINDER CHECK TIMING: '+ (Date.now()-lastDate));
  //   // console.log("Current time: "+ Date.now());
  //   lastDate = Date.now();
  //   setTimeout(arguments.callee, 2000);
  // }, 2000);
});

////////////////////////////////////////////////////////////////////////////
//message handling
client.on('messageCreate', msg => {
  var msgTxt = msg.content;
  //check for roll call before ignoring bots
  //also check if author of the roll call is from self
  // if (msgTxt.substring(0,10) == "[rollCall]") {
  if (msgTxt.substring(0,10) == "[rollCall]" && msg.author.id !== client.user.id) {
    // console.log("roll call msg detected");
    rollCall.respond(msg);
  }

  // ignore self?
  // if (msg.author.id !== client.user.id) return;

  //ignore other bots for now
  if(msg.author.bot) return;




  //handle DMs
  // if(msg.channel.type == "dm"){
  //   // console.log(msg.author.username);
  //   // console.log(typeof msg.author.username);
  //
  //   //PRIVATE KREETCHA COMANDS
  //   //check if it is me
  //   if (msg.author.id === config.adminId) {
  //     //confirmed pm is from me
  //     console.log('PM from kreetcha:' + msgTxt);
  //
  //     //PRIVATE COMMANDS
  //     if (msgTxt.substring(0,1) == '!') {
  //       //say
  //       if (msgTxt.substring(1,4) == 'say') {
  //         let thingToSay = msgTxt.substring(5);
  //         console.log('Kreetcha told Marvin to say: ' + thingToSay);
  //         msg.reply('dude you need to fix admin commands!!! >.< ')
  //         //dsMainChannel.send(thingToSay);
  //       }
  //
  //     }
  //     // end of ! commands
  //   }
  //   // end of if kreetcha
  // }



  //PUBLIC COMMANDS

  //check for ! for commands
  // var cmdPrefix = config.cmdPrefix
  if (msgTxt.substring(0,1) == cmdPrefix) {

    //const cmdArgs = msgTxt.slice(1).trim().split(/ +/g);
		//const cmdArgs = msgTxt.slice(1).trim().split(/ +/g);
		// const cmdArgs = msgTxt.slice(1).trim().split(/( +)|(\n+)/g);

		const cmdArgs = msgTxt.slice(1).trim().split(/\s+/g);

    const command = cmdArgs.shift().toLowerCase();
		// console.log(cmdArgs);
		// console.log(command);

    //split(' ') will only split at one space, while split(/ +/g) will split multiple spacces
    //BUG REMOVIN MORE THAN ONE SPACE WHEN PARSING COMMANDS WILL CAUSE ISSUES WITH CIPHER TOOLS BEST TO DO JUST ONE.
    commandHandler(db, config, client, msg, command, cmdArgs);

    //end of ! COMMANDS
  }

   if (msgTxt.substring(0,5) === "/roll" || msgTxt.substring(0,5) === config.cmdPrefix+"roll" && msgTxt.length == 5) {
    //reply on main channel or to message?!?!?!?!!?!?!?!??!?!!?!?!?!?!!??!!?!!?!?!?!?!?!!!?!?!
    if(msg.channel.type == "DM"){
      msg.reply('<@' + msg.author.id + '> rolls ' + chance.integer({ min: 1, max: 100})+' (1-100)');
    } else {
      msg.channel.send('<@' + msg.author.id + '> rolls ' + chance.integer({ min: 1, max: 100})+' (1-100)');
    }

  } else if (msgTxt.substring(0,7) == "/advice" ||   msgTxt.substring(0,7) == "!advice") {
    //if !help is on a public channel, reply with msg and then DM helplist
    if (msg.channel.type != "DM") {
      msg.channel.send('<@' + msg.author.id + '> I\'d give you advice, but you wouldn\'t listen.\n No one ever does...');
    }

  }


  //easter egg phrases
  //NEED TO MAKE MOST OF THESE ONLY ON MAIN CHANNEL!!!! (so bot can listen and organize other channels but not talk on them)
  if(msgTxt.includes("check out") || msgTxt.includes("check this out") ) {

    //add
      //check that out?
     //roll a slim chance to reply

     if (chance.integer({min:1, max:42}) == 42) {
       msg.reply('^^I\'ve seen it. It\'s rubbish.');
     }


  }

  // if(msg.content === '!ping') {
  // msg.reply('pong!');
  // }


})
/////////////////////////////////////////////////////////////

//HANDLE Join
//NEEDSS TESTING!!


// client.on('guildMemberAdd', (member) =>{
//
//   let membJoinWelcomeChannel =member.guild.channels.find('name', 'general');
//   if (membJoinWelcomeChannel) {
//     membJoinWelcomeChannel.send('Hello, <@' + member.user.id + '>.  I\'ve been ordered to welcome you upon arival...\nHere I am, brain the size of a planet and they ask me to welcome you.  Call that job satisfaction?  \'Cos I don\'t.');
//   }
//   //dsMainChannel.send( 'Hello, <@' + member.user.id + '>.  I\'ve been ordered to welcome you upon arival...\nHere I am, brain the size of a planet and they ask me to welcome you.  Call that job satisfaction?  \'Cos I don\'t.');
// });

//ERROR HANDLER FOR DISCORD CLIENT
client.on('error', err =>{
  console.log("ERROR: discord client error event @ "+new Date());
  console.error(err);
  process.exit(1);
});
//Login to Discord
client.login(token);
