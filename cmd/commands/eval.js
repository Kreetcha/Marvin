var dbot = require('../../features/dbot/dbot.js')


var splitOptions = {
	"maxLength": 1800,
  "prepend": "`EVAL OUTPUT CONTINUED:`\n```js\n",
  "append": "```",
	"char": "\n"
};
var errSplitOptions = {
	"maxLength": 1800,
  "prepend": "`EVAL ERROR OUTPUT CONTINUED:`\n```js\n",
  "append": "```",
	"char": "\n"

}
module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (msg.author.id === config.adminId) {

    //if (cmdArgs[0] !== undefined) {
		if (cmdArgs.length > 0 !== undefined) {


      console.log('EVAL!!!');
      var evalIn = cmdArgs.join(" ");
      console.log('EVAL IN: ', evalIn);
      try {
        let evalOut = require('util').inspect(eval(evalIn))
        // if (evalOut.length > 1950) {
        //   //need to split output into multiple messages after 2000 char over flow here
        //   evalOut = evalOut.substr(0,1950);
        // }
        //client.users.get(config.adminId).send("`EVAL INPUT`\n```js\n"+evalIn+"```")
				dbot.replyMessageSplit(msg, splitOptions, "`EVAL OUTPUT:`\n```js\n"+evalOut+"```")

        // msg.reply("`EVAL OUTPUT:`\n```js\n"+evalOut+"```", {'split': splitOptions});
      } catch (err) {
        console.log('EVAL ERROR: ', err);

				dbot.replyMessageSplit(msg, errSplitOptions, "EVAL ERROR! >.<\n```js\n"+err+ "```")
        // msg.channel.send("EVAL ERROR! >.<\n```js\n"+err+ "```", {'split':errSplitOptions});
      }
    }

  }
  return
}
