var dbot = require('../../features/dbot/dbot.js')

module.exports = function (db, config, client, msg, command, cmdArgs) {
	if (msg.author.id !== config.adminId) return;
	
	//msg.channel.send("<@"+msg.author.id+'> pong! The websocket heartbeat is '+Math.round(client.ws.ping)+"ms.");
	longMessage = ""
	for (var i = 0; i < 100; i++){

		for (var x = 0; x < 100; x++) {
			longMessage += `#`
		}

		//longMessage += `\n`
	}
	// var splitOptions = {
	// 	"maxLength": 1800,
	//   "prepend": "`SPLIT TEST OUTPUT CONTINUED:`\n```js\n",
	// 	"char": ["\n", " "],
	//   "append": "```",
	// };


	var splitOptions = {
		"maxLength": 1800,
		"prepend": "`SPLIT TEST OUTPUT CONTINUED:`\n```js\n",
		"char": `\n`,
		"append": "```",
	};
	// dbot.dmAdmin("yay!")
	dbot.sendMessageSplit(msg.channel, splitOptions, "`SPLIT TEST OUTPUT:`\n```js\n"+longMessage+"```")

	return
}
