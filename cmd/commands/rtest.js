module.exports = function (db, config, client, msg, command, cmdArgs) {
    // client.guilds.forEach((guild) =>{
    //   // guildNames.push(guild.name);
    //   // console.log("============================");
    //   // console.log("Guild: "+guild.name);
    //   guild.channels.forEach((channel) =>{
    //     if ((channel.type == "text") && (channel.name == "rtest")) {
    //       if (channel.permissionsFor(msg.guild.me).has("SEND_MESSAGES")) {
    //         // console.log(channel);
    //         channel.send("hi")
    //       }
    //     }
    //   });
    // });
  // const request = require('request')
  // request('https://www.reddit.com/r/redditdev/top.json', { json: true }, (err, res, body) => {
  //   if (err) { return console.log(err); }
  //   // console.log(err);
  //   console.log(res);
  //   console.log(body.url);
  //   console.log(body.explanation);
  // });
  //
  // function savePostToDb(dbDoc){
  // if (typeof db == "undefined") {
  //   return {"ok":false, "err":"db undefined when saving fgos Post (redditAlert) @ "+ new Date()}
  // }
  // db.collection('redditAlert').insertOne(doc, function(err, res){
  //   if (err) {
  //     console.log("ERROR: fgos Post (redditAlert) db save failed @ "+new Date());
  //     console.log(err);
  //     return {"ok":false, "err":err}
  //   }
  //   console.log("fgos Post (redditAlert) saved ^.^");
  // });
  //   return {ok:True}
  // }
  // function postAlertAndSave(channel, myData){
  //   channel.send("```js\n\n"+JSON.stringify(myData,0,2)+"\n```").then((sentMsg) => {
  //     let sentMsgID = sentMsg.id
  //     console.log("~~~~ msg callback ~~~~");
  //     console.log("sentMsg:"+ sentMsg);
  //     console.log("sentMsgID:"+ sentMsgID);
  //     //store doc to db
  //     let dbDoc = {
  //       type:"freeGamesOnSteam",
  //       discordAlertMsgID: sentMsgID,
  //       data: myData
  //     }
  //     let saveTry = savePostToDb(dbDoc)
  //     if (saveTry.ok == False) {
  //       //handle db save error :(
  //       //delete last post? how to handle this event?
  //     } else {
  //
  //     }
  //   });
  // }

/*
======= OPTION 1 =======

if (post is not in db){

//1 create embed and a db doc for this post

	for each channel named fgos in all guilds{


		sendEmbed(sweetEmbed).then( (msg) =>{
				appendMsgIdToPostDocArray
		})
	}


} else if (post is in db){
	handle ended event

}


======= OPTION 2 =======

*/


  const request = require('request')
  const Discord = require('discord.js');
  const options = {
  	url: 'https://www.reddit.com/r/FreeGamesOnSteam/new.json',
  	method:"GET",
  	headers: {
  		'Accept': 'application/json',
  		'Accept-Charset': 'utf-8',
  		'User-Agent': 'my-reddit-client'
  	}
  }
  if (typeof db == "undefined") {
    console.log("db not connected (undefined)!!")
    console.log("ERROR: error retrieving fgos reddit @ "+new Date());
    // msg.author.send(msg.author+"db not connected ):\n Please retry or contact admin if this persists")
    return
  }
  //  request(options, { json: true }, (err, res, body) => {
  request(options, (err, res, body) => {
  	//  request('https://www.reddit.com/r/redditdev/top.json', { json: true }, (err, res, body) => {
  	if (err) {
      console.log("ERROR: request for fgos reddit json failed! @ "+ new Date());
      console.log(err);
      return
    }
  	// console.log(err);
  	//    console.log("res:");
  	//  console.log(res);
  	//console.log(body.url);
  	// console.log(body.explanation);
  	//console.log(res.body)
  	var listObj = JSON.parse(res.body)
    if (typeof listObj.data == "undefined") {
      console.log("ERROR: request to fgos json didn't result in error but has no data :( @ "+ new Date() );
      return
    }
  	//console.log(JSON.stringify(listObj,0,2))
  	var postsCount = listObj.dist
    var whiteList = [
      "store.steampowered.com",
      "humblebundle.com"
    ]
  	console.log(typeof listObj.data.children)
  	for (var post of listObj.data.children){
      // if (whiteList.includes(post.data.domain) && post.data.link_flair_css_class != "ended") {
      // if (whiteList.includes(post.data.domain)) {
      if (whiteList.includes(post.data.domain) && !(post.data.link_flair_css_class.includes("ended"))) {

        console.log("=================reddiAlert (fgos)==================");
        // console.log(post)
        var myData = {
          title: post.data.title,
          author: post.data.author,
          domain: post.data.domain,
          thumbnail: post.data.thumbnail,
          postID: post.data.id,
          link_flair_css_class: post.data.link_flair_css_class,
          content: post.data.selftext,
          linkToPost: post.data.permalink,
          linkFromPost: post.data.url,
          created_utc: post.data.created_utc

        }
        console.log(myData);
        //check if post is in db

        var postDbCheckCallback = function(err, rez) {
          // var thisPostDoc = {}
          var thisPostDoc = this.postDoc
          var channelsToPostOn = []
          if (rez.length == 0) {
            client.guilds.forEach((guild) =>{
              // guildNames.push(guild.name);
              // console.log("============================");
              // console.log("Guild: "+guild.name);
              guild.channels.forEach((channel) =>{
                if ((channel.type == "text") && (channel.name == "fgos") && (channel.permissionsFor(guild.me).has("SEND_MESSAGES"))) {
                  channelsToPostOn.push(channel)
                  // console.log("channel.id: "+channel.id);
                }
              });
            });
            // console.log("channelsToPostOn: "+channelsToPostOn);
            let promiseArr = channelsToPostOn.map(function(channel){
              let embedToSend = new Discord.RichEmbed()
                .setTitle(thisPostDoc.title)
                .setURL("https://www.reddit.com"+thisPostDoc.linkToPost)
                .setThumbnail(thisPostDoc.thumbnail)
                .addField('Domain', thisPostDoc.domain, true)
                .addField('Offer', thisPostDoc.linkFromPost, true)
                .setDescription(thisPostDoc.content)
                // .setColor('#df0000')
                .setColor('#00df00')
              return channel.send(embedToSend).then((sentMsg) => {
                return {"guildId":sentMsg.guild.id, "channelId":sentMsg.channel.id, "msgId":sentMsg.id}
              });
            });
            Promise.all(promiseArr).then((results) =>{
              // console.log("Sent msg ids??:");
              // console.log(results);
              //NOW POST DOC TO DB!! (:
              let dbDoc = {
                type:"freeGamesOnSteam",
                discordAlertMsgs: results,
                data: thisPostDoc
              }
              // console.log(dbDoc);
              db.collection('redditAlert').insertOne(dbDoc, (err,res) =>{
                if (err) {
                  console.log("ERROR: fgos post save failed!! @ "+new Date());
                  console.log(err);
                  // return {"ok":false, "err":err}
                }
                // console.log("Post doc Saved to db!!");
                console.log(`fgos Post doc Saved to db!! postID: ${res.ops[0].data.postID}`);

                 // else {
                 //   return {"ok":true}
                 // }
              });
            }).catch((err) =>{
              console.log("ERROR: in promiseArr in redditAlert @ "+new Date());
              console.log(err);
            });
          } else if (rez.length == 1) {
            //post already in db
            console.log(`fgos Post Already in db!! postID: ${thisPostDoc.postID}`);
            //check if db post was not ended but new post is ended
            var newPostCssClass = thisPostDoc.link_flair_css_class
            var oldPostCssClass = rez[0].data.link_flair_css_class;
            if ((newPostCssClass != oldPostCssClass) && (newPostCssClass == "ended")) {
              //^^ need a better check for this
              //now edit post
              console.log(`fgos offer has now ended!! postID: ${rez[0].data.postID}`);
              rez[0].discordAlertMsgs.forEach((msgIds) =>{
                // var msgToEdit = client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).messages.get(msgIds.msgId)
                var editMsgCallback = function(postMsg){
                  console.log("~~~~~~~~~~~~~~~~");
                  // console.log(postMsg.embeds);
                  const oldEmbed = postMsg.embeds[0]
                  const oldTitle = oldEmbed.title
                  const newTitle = "~~"+oldTitle+"(ENDED)~~"
                  const  newEmbed = new Discord.RichEmbed(oldEmbed)
                  .setColor("#df0000")
                  .setTitle(newTitle)
                  .setColor("#df0000")
                   console.log(newEmbed);
                  postMsg.edit(newEmbed);
                  //now update db
                  console.log(this.postDoc);
                  // db.collection('redditAlert').updateOne({"data.postID": rez[0].data.postID}, {$set:{"data.link_flair_css_class": "ended"}}, (err,res) =>{
                  //   console.log("fgos ended event, db updated! @ "+new Date());
                  // });

                }
                client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).fetchMessage(msgIds.msgId).then(editMsgCallback.bind({"postDoc": thisPostDoc}))

                // client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).fetchMessage(msgIds.msgId).then((postMsg) =>{
                //   console.log("~~~~~~~~~~~~~~~~");
                //   // console.log(postMsg.embeds);
                //   const oldEmbed = postMsg.embeds[0]
                //   const oldTitle = oldEmbed.title
                //   const newTitle = "~~"+oldTitle+"(ENDED)~~"
                //   const  newEmbed = new Discord.RichEmbed(oldEmbed)
                //   .setColor("#df0000")
                //   .setTitle(newTitle)
                //   .setColor("#df0000")
                //   // console.log(newEmbed);
                //   postMsg.edit(newEmbed);
                //   //now update db
                //   db.collection('redditAlert').update({"data.postID": rez[0].data.postID}, {$set:{"data.link_flair_css_class": }})
                //
                // });
                // client.guilds.get(msgIds.guildId).channels.get(msgIds.channelId).fetchMessage(msgIds.msgId).then((postMsg) =>{
                //   console.log("~~~~~~~~~~~~~~~~");
                //   // console.log(postMsg.embeds);
                //   const oldEmbed = postMsg.embeds[0]
                //   const oldTitle = oldEmbed.title
                //   const newTitle = "~~"+oldTitle+"(ENDED)~~"
                //   const  newEmbed = new Discord.RichEmbed(oldEmbed)
                //   .setColor("#df0000")
                //   .setTitle(newTitle)
                //   .setColor("#df0000")
                //   // console.log(newEmbed);
                //   postMsg.edit(newEmbed);
                //   //now update db
                //   db.collection('redditAlert').update({"data.postID": rez[0].data.postID}, {$set:{"data.link_flair_css_class": }})
                //
                // });

              });
              db.collection('redditAlert').updateOne({"data.postID": rez[0].data.postID}, {$set:{"data.link_flair_css_class": "ended"}}, (err,res) =>{
                console.log("fgos ended event, db updated! @ "+new Date());
              });

            }
            // console.log(rez[0]);
            // console.log("post already in db!!");
          } else if (rez.length > 1) {
            //DUPLICATE DOCUMENT ERROR!!!!!
            console.log("ERROR: DUPLICATE fgos DOCUMENT ERROR!!!!! @ "+new Date());
          }
        }

        // postDbCheckCallback.bind({"postDoc": myData})
        db.collection('redditAlert').find({"data.postID": post.data.id}).toArray(postDbCheckCallback.bind({"postDoc": myData}))

        // db.collection('redditAlert').find({"data.postID": post.data.id}).toArray((err, rez)=> {
        //   var thisPostDoc = {}
        //   thisPostDoc = myData
        //   var channelsToPostOn = []
        //   if (rez.length == 0) {
        //     client.guilds.forEach((guild) =>{
        //       // guildNames.push(guild.name);
        //       // console.log("============================");
        //       console.log("Guild: "+guild.name);
        //       guild.channels.forEach((channel) =>{
        //         if ((channel.type == "text") && (channel.name == "fgos") && (channel.permissionsFor(guild.me).has("SEND_MESSAGES"))) {
        //           channelsToPostOn.push(channel)
        //           console.log("channel.id: "+channel.id);
        //         }
        //       });
        //     });
        //     console.log("channelsToPostOn: "+channelsToPostOn);
        //     let promiseArr = channelsToPostOn.map(function(channel){
        //       let embedToSend = new Discord.RichEmbed()
        //         .setTitle(thisPostDoc.title)
        //         .setURL("https://www.reddit.com"+thisPostDoc.linkToPost)
        //         .setThumbnail(thisPostDoc.thumbnail)
        //         .addField('Domain', thisPostDoc.domain, true)
        //         .addField('Offer', thisPostDoc.linkFromPost, true)
        //         .setDescription(thisPostDoc.content)
        //         .setColor('#df0000')
        //       return channel.send(embedToSend).then((sentMsg) => {
        //         return sentMsg.id
        //       });
        //     });
        //     Promise.all(promiseArr).then((results) =>{
        //       console.log("Sent msg ids??:");
        //       console.log(results);
        //     }).catch((err) =>{
        //       console.log("ERROR: in promiseArr in redditAlert @ "+new Date());
        //       console.log(err);
        //     });
        //   } else if (rez.length == 1) {
        //     //post already in db
        //   } else if (rez.length > 1) {
        //     //DUPLICATE DOCUMENT ERROR!!!!!
        //   }
        // });

        // db.collection('redditAlert').find({"data.postID": post.data.id}).toArray((err, rez)=> {
        //   if (rez.length == 0) {
        //
        //     let promiseArrGuilds = client.guilds.map( (guild) =>{
        //       function checkChannel(channel)
        //       function checkGuild(guild){
        //         let promiseArrChannels = guild.channels.map( (channel) =>{
        //
        //         });
        //         if (true) {
        //
        //         }
        //       }
        //       return checkGuild(guild).then((res) => {
        //         return res
        //       });
        //     })
        //
        //   } else if (rez.length == 1) {
        //     //post already in db
        //   } else if (rez.length > 1) {
        //     //DUPLICATE DOCUMENT ERROR!!!!!
        //   }
        // });

            // db.collection('redditAlert').find({"data.postID": post.data.id}).toArray((err, rez)=> {
            //   if (rez.length == 0) {
            //     //post not in db
            //
            //     //itterate over guilds to find channels named fgos
            //     var alertMsgIds = []
            //     var findChannelsAndAlert = new Promise ((resolve, reject) => {
            //       client.guilds.forEach((guild) =>{
            //         // guildNames.push(guild.name);
            //         // console.log("============================");
            //         console.log("Guild: "+guild.name);
            //         guild.channels.forEach((channel) =>{
            //           if ((channel.type == "text") && (channel.name == "fgos")) {
            //             // if (channel.permissionsFor(msg.guild.me).has("SEND_MESSAGES")) {
            //               if (channel.permissionsFor(guild.me).has("SEND_MESSAGES")) {
            //
            //                 // console.log(channel);
            //                 //post alert and save to db
            //                 // postAlertAndSave(channel, myData)
            //                 // let postToSend = "```js\n\n"+JSON.stringify(myData,0,2)+"\n```"
            //                 let embedToSend = new Discord.RichEmbed()
            //                 .setTitle(myData.title)
            //                 .setURL("https://www.reddit.com"+myData.linkToPost)
            //                 .setThumbnail(myData.thumbnail)
            //                 .addField('Domain', myData.domain, true)
            //                 .addField('Offer', myData.linkFromPost, true)
            //                 .setDescription(myData.content)
            //                 .setColor('#df0000')
            //                 channel.send(embedToSend).then((sentMsg) => {
            //                   let sentMsgID = sentMsg.id
            //                   console.log("~~~~ msg callback ~~~~");
            //                   console.log("sentMsg:"+ sentMsg);
            //                   console.log("sentMsgID:"+ sentMsgID);
            //                   //save post ID to array
            //                   // console.log(psst.data.);
            //                   alertMsgIds.push(sentMsg.id)
            //                   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //                   // ^^ need to find way to save doc after all messages are sent
            //                   //OR save doc to db without any ids, THEN itterate guilds and send messages,
            //                   // on each saved message push the msg id to an array in the mongo doc
            //                 });
            //               }
            //             }
            //           });
            //         });
            //         resolve()
            //     });
            //     findChannelsAndAlert.then(() => {
            //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //       console.log("msgIds: "+ alertMsgIds);
            //     });
            //     //create doc for db
            //     let dbDoc = {
            //       type:"freeGamesOnSteam",
            //       discordAlertMsgIDs: [],
            //       data: myData
            //     }
            //     console.log("dbDoc: ");
            //     console.log(dbDoc);
            //   } else if (rez.length ==1) {
            //     //post already in db
            //   } else if (rez.length > 1) {
            //     //DUPLICATE DOCUMENT ERROR!!!!!
            //   }
            // });
            //

        // db.collection('redditAlert').find({"data.postID": post.data.id}).toArray((err, rez)=> {
        //   if (rez.length == 0) {
        //     //post not in db
        //
        //     //itterate over guilds to find channels named fgos
        //     var alertMsgIds = []
        //     var findChannelsAndAlert = new Promise ((resolve, reject) => {
        //       client.guilds.forEach((guild) =>{
        //         // guildNames.push(guild.name);
        //         // console.log("============================");
        //         console.log("Guild: "+guild.name);
        //         guild.channels.forEach((channel) =>{
        //           if ((channel.type == "text") && (channel.name == "fgos")) {
        //             // if (channel.permissionsFor(msg.guild.me).has("SEND_MESSAGES")) {
        //               if (channel.permissionsFor(guild.me).has("SEND_MESSAGES")) {
        //
        //                 // console.log(channel);
        //                 //post alert and save to db
        //                 // postAlertAndSave(channel, myData)
        //                 // let postToSend = "```js\n\n"+JSON.stringify(myData,0,2)+"\n```"
        //                 let embedToSend = new Discord.RichEmbed()
        //                 .setTitle(myData.title)
        //                 .setURL("https://www.reddit.com"+myData.linkToPost)
        //                 .setThumbnail(myData.thumbnail)
        //                 .addField('Domain', myData.domain, true)
        //                 .addField('Offer', myData.linkFromPost, true)
        //                 .setDescription(myData.content)
        //                 .setColor('#df0000')
        //                 channel.send(embedToSend).then((sentMsg) => {
        //                   let sentMsgID = sentMsg.id
        //                   console.log("~~~~ msg callback ~~~~");
        //                   console.log("sentMsg:"+ sentMsg);
        //                   console.log("sentMsgID:"+ sentMsgID);
        //                   //save post ID to array
        //                   // console.log(psst.data.);
        //                   alertMsgIds.push(sentMsg.id)
        //                   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //                   // ^^ need to find way to save doc after all messages are sent
        //                   //OR save doc to db without any ids, THEN itterate guilds and send messages,
        //                   // on each saved message push the msg id to an array in the mongo doc
        //                 });
        //               }
        //             }
        //           });
        //         });
        //         resolve()
        //     });
        //     findChannelsAndAlert.then(() => {
        //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //       console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //       console.log("msgIds: "+ alertMsgIds);
        //     });
        //     //create doc for db
        //     let dbDoc = {
        //       type:"freeGamesOnSteam",
        //       discordAlertMsgIDs: [],
        //       data: myData
        //     }
        //     console.log("dbDoc: ");
        //     console.log(dbDoc);
        //   } else if (rez.length ==1) {
        //     //post already in db
        //   } else if (rez.length > 1) {
        //     //DUPLICATE DOCUMENT ERROR!!!!!
        //   }
        // });


        // var postIDs = []
        //if not post message
        // client.guilds.forEach((guild) =>{
        //   // guildNames.push(guild.name);
        //   // console.log("============================");
        //   // console.log("Guild: "+guild.name);
        //   guild.channels.forEach((channel) =>{
        //     if ((channel.type == "text") && (channel.name == "fgos")) {
        //       // if (channel.permissionsFor(msg.guild.me).has("SEND_MESSAGES")) {
        //       if (channel.permissionsFor(guild.me).has("SEND_MESSAGES")) {
        //
        //         // console.log(channel);
        //         //post alert and save to db
        //         // postAlertAndSave(channel, myData)
        //         // let postToSend = "```js\n\n"+JSON.stringify(myData,0,2)+"\n```"
        //         let embedToSend = new Discord.RichEmbed()
        //             .setTitle(myData.title)
        //             .setURL("https://www.reddit.com"+myData.linkToPost)
        //             .setThumbnail(myData.thumbnail)
        //             .addField('Domain', myData.domain, true)
        //             .addField('Offer', myData.linkFromPost, true)
        //             .setDescription(myData.content)
        //             .setColor('#df0000')
        //         channel.send(embedToSend).then((sentMsg) => {
        //           let sentMsgID = sentMsg.id
        //           console.log("~~~~ msg callback ~~~~");
        //           console.log("sentMsg:"+ sentMsg);
        //           console.log("sentMsgID:"+ sentMsgID);
        //           //save post ID to array
        //           // console.log(psst.data.);
        //           postIDs.push(sentMsg.id)
        //           //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //           // ^^ need to find way to save doc after all messages are sent
        //           //OR save doc to db without any ids, THEN itterate guilds and send messages,
        //           // on each saved message push the msg id to an array in the mongo doc
        //         });
        //       }
        //     }
        //   });
        // });

        //create doc for db and save
        // let dbDoc = {
        //   type:"freeGamesOnSteam",
        //   discordAlertMsgIDs: postIDs,
        //   data: myData
        // }
        // console.log("dbDoc: ");
        // console.log(dbDoc);
      }
      // console.log(post.data.domain);

  	}

  });

  return
}
