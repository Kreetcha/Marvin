module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (msg.author.id !== config.adminId) return;

  require('./../../features/rollCall/rollCall.js').rollcallignoreCmd(config, client, msg, cmdArgs);

  return
}
