var Chance = require('chance'),
    chance = new Chance();

module.exports = function (db, config, client, msg, command, cmdArgs) {
  //dice
  let diceMax = cmdArgs[0];
  //max allowed dice sides
  //https://chancejs.com/basics/integer.html
  let maxInput = 9007199254740990
  //check for NaN
  if (isNaN(diceMax) === false) {
    diceMax = parseInt(diceMax);
    if (diceMax === 0) {
      msg.reply('I\'ve calculated your chance of survival, but I don\'t think you\'ll like it.');

      // if(msg.channel.type == "dm"){
      //   //remove mention here?
      //   //msg.reply('<@' + msg.author.id + '> I could calculate your chance of survival, but you won\'t like it.');
      //   msg.reply('I\'ve calculated your chance of survival, but I don\'t think you\'ll like it.');
      // } else {
      //   msg.channel.send('<@' + msg.author.id + '> I could calculate your chance of survival, but you won\'t like it.');
      // }


    } else if(diceMax < 0){
      msg.reply( 'The dice command requires a non-zero positive integer, like this:\n`<!d 20>`');

    } else if (diceMax > maxInput){
      msg.reply('I\'ve calculated your chance of survival, but I don\'t think you\'ll like it.\n`( too many sides too your dice >.< )`');


    } else { // OK GOOD
			msg.reply('<@' + msg.author.id + '>\'s d'+diceMax+' rolled a '+ chance.integer({ min: 1, max: diceMax}));

      // if(msg.channel.type == "DM"){
      //   //remove mention here?
      //   msg.reply('<@' + msg.author.id + '>\'s d'+diceMax+' rolled a '+ chance.integer({ min: 1, max: diceMax}));
      // } else {
      //   msg.channel.send('<@' + msg.author.id + '>\'s d'+diceMax+' rolled a '+ chance.integer({ min: 1, max: diceMax}));
      // }


    }


  } else {
    if(msg.channel.type == "DM"){
        msg.reply( 'The dice command requires a non-zero positive integer, like this:\n`<!d 20>`');
      // msg.reply('<@' + msg.author.id + '>, the dice command requires a non-zero positive integer, like this:\n<!d 20>');
    } else {
        msg.reply( 'The dice command requires a non-zero positive integer, like this:\n`<!d 20>`');
      // msg.reply('the dice command requires a non-zero integer, like this:\n<!d 20>');
    }
  }

  return
}
