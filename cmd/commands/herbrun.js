const reminders = require('./../../features/reminders/reminders.js');

module.exports = function (db, config, client, msg, command, cmdArgs) {
  // msg.channel.send(msg.author+' pong! The websocket heartbeat is '+Math.round(client.ping)+"ms.");
  let timeOfReminder = Date.now()+(4800000)
  let timeString = new Date(timeOfReminder).toLocaleString()
  timeString = "` @ "+timeString+" CST (UTC-6)`"


  // CHECK IF USER IS ALREADY ON BIRDHOUSE COOLDOWN AND PROMPT FOR RESET HERE!
  // create reminder
  let recipientName = msg.author.username;
  let recipientId = msg.author.id;

  let reminderTxt = "Harvest your herbs! ^.^"

  // set reminder to 15 minutes from now
  // let timeOfReminder = Date.now()+(900000)
  // let time = Date.now()+(900000)
  var saveTry = reminders.createReminder(db, recipientName, recipientId, reminderTxt, timeOfReminder);
  if (saveTry.ok == false) {
    console.log("ERROR: shift cmd save gave ok:false @ "+ new Date());
    console.log(saveTry.err);
    msg.reply("There was an error saving your shift reminder to db!\nPlease try again and contact admin if issue persists.")
    return
  }
  msg.reply("I'll remind you to do your next herbrun\n@ "+ new Date(timeOfReminder))
  return
}
