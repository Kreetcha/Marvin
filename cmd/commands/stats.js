module.exports = function (db, config, client, msg, command, cmdArgs) {
  // if (msg.author.id !== config.adminId) return;
  const Discord = require('discord.js');
  //get uptime String
  //https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
  function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
  var uptimeSeconds = process.uptime()
  var upDays = Math.floor(uptimeSeconds / 86400);
  var upHours = Math.floor((uptimeSeconds - (upDays * 86400)) / 3600);
  var upMinutes = Math.floor((uptimeSeconds - ((upHours * 3600) + (upDays * 86400)))  / 60);
  var upSeconds = Math.floor(uptimeSeconds - ((upDays * 86400) + (upHours * 3600) + (upMinutes * 60)) )

 //  var upH = Math.floor(uptimeSeconds /   (6060));
 // var upM = Math.floor(uptimeSeconds % (6060) / 60);
 // var upS = Math.floor(uptimeSeconds % 60);
 // var uptimeStr = upDays+"days "+upHours+"h "+upMinutes+"m "+upSeconds+"s"
 var uptimeStr = ""
 if (upDays > 0) {
   uptimeStr += upDays+"days "
 }
uptimeStr += upHours+"h "+upMinutes+"m "+upSeconds+"s"
//get mem usage to format later with formatBytes
var memUsage = process.memoryUsage().rss
//get db Status
var dbStatus = ""
if (typeof db == "undefined") {
  dbStatus = "NC"
} else {
  dbStatus = "OK"
}
  // var status
  const statusEmbed = new Discord.MessageEmbed()
      .setColor("0x88BC50")
      .setTitle('Marvin Stats')
      .addField('Uptime', uptimeStr, true)
      .addField('memUsage', formatBytes(memUsage), true)
      .addField('dbStatus', dbStatus, true)

      // .setAuthor('Marvin', client.user.avatarURL)
      // .setImage(client.user.avatarURL)
      .setThumbnail('https://cdn.discordapp.com/icons/450345744650469378/cdc4de949bf8b025782fb7626dba3346.jpg')
  msg.channel.send({embeds:[statusEmbed]})
  return
}
