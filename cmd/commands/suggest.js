module.exports = function (db, config, client, msg, command, cmdArgs) {
  //send suggestion to admin
  var suggMsg = cmdArgs.join(" ");
  console.log("suggMsg:"+suggMsg);
  console.log("suggMsg Len:"+suggMsg.length);
  var suggMsgMinLength = 7; // WHAT MIN LENGTH???
  if (suggMsg.length == 0) {
    // too short
  } else if(suggMsg.length <= suggMsgMinLength) {
    // min length not reached

  }
  var suggReport = "<@"msg.author.id+"> issued a `Suggestion!`\n```"+suggMsg+"```";
  var suggReportCopy = msg.author+", here is a copy of your recently issued `Suggestion!`\n```"+suggMsg+"```";
  client.users.cache.get(config.adminId).send(suggReport);
  msg.author.send(suggReportCopy);
  return
}
