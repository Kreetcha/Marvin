var dbot = require('../../features/dbot/dbot.js')


module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (msg.author.id !== config.adminId) return;
  console.log("Running admin command (channels) @ "+new Date());
  var outText = "**__Channels__**\n"
  client.guilds.cache.forEach((guild) =>{

    var channelCount = 0;

    var channelListStr = ""
    guild.channels.cache.forEach((channel) =>{
      if (channel.type != "GUILD_CATEGORY") {
        channelListStr += "Channel: `"+channel.name+"` ("+channel.type+")\n"
        // console.log("channel: "+channel.name);
        // console.log("type: "+channel.type);
        channelCount++;
      }

    });

    // guildNames.push(guild.name);
    outText+= "\n=======================\n"
    outText += "Guild: __"+guild.name+"__\n"
    outText += "Channels: "+channelCount+"\n\n"
    outText += channelListStr
  });
  // msg.channel.send(output);
	var splitOptions = {
		"maxLength": 1800,
		"char": "\n",
	};
	dbot.sendMessageSplit(msg.author.dmChannel, splitOptions, outText)
  // msg.author.send(outText, {'split': true});
  return
}
