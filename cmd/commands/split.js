const cTools = require('./../../features/cTools/cTools.js');

module.exports = function(db, config, client, msg, command, cmdArgs){
  var inBase = cmdArgs[0];
  var stringAfterSplit = cTools.splitString(inBase, cmdArgs.slice(1).join(" "));

  var splitOutput = "\n`Split string into chunks of "+inBase+":`\n```\n"+stringAfterSplit+"```";
  if (splitOutput.length > 1950) {
    msg.channel.send(msg.author+" `ERROR`\n```\n splitString output Larger than message length limit.\n try splitting up input into smaller chunks");
    return
  }
  msg.reply(splitOutput);
  return
}
