const reminders = require('./../../features/reminders/reminders.js');

module.exports = function (db, config, client, msg, command, cmdArgs) {
  // msg.channel.send(msg.author+' pong! The websocket heartbeat is '+Math.round(client.ping)+"ms.");
  let timeOfReminder = Date.now()+(900000)
  let timeString = new Date(timeOfReminder).toLocaleString()
  timeString = "` @ "+timeString+" CST (UTC-6)`"
  if (cmdArgs.length > 0){
    // script name was given
    console.log("script name input was givin: "+cmdArgs)
    var scripts = cmdArgs.join(" ");
    var reminderTxt = "Your "+scripts+" script(s) have finished shifting!"
    // var reminderTxt = ""
    var replyMsg = "Ok. I'll remind you when your "+scripts+" scripts are finished shifting.\n"+timeString

  } else {
    //script name wasn't given
    var reminderTxt = "Your script(s) have finished shifting!";
    var replyMsg = "Ok. I'll remind you when your scripts are finished shifting.\n"+timeString;

  }

  // create reminder

  let recipientName = msg.author.username;
  let recipientId = msg.author.id;

  // set reminder to 15 minutes from now
  // let timeOfReminder = Date.now()+(900000)
  // let time = Date.now()+(900000)
  var saveTry = reminders.createReminder(db, recipientName, recipientId, reminderTxt, timeOfReminder);
  if (saveTry.ok == false) {
    console.log("ERROR: shift cmd save gave ok:false @ "+ new Date());
    console.log(saveTry.err);
    msg.reply("There was an error saving your shift reminder to db!\nPlease try again and contact admin if issue persists.")
    return
  }
  msg.reply(replyMsg)
  return
}
