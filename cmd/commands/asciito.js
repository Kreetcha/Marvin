const cTools = require('./../../features/cTools/cTools.js');

var splitOptions = {
  "prepend": "ASCIITO OUTPUT CONTINUED:`\n```js\n",
  "append": "```",
};

module.exports = function (db, config, client, msg, command, cmdArgs) {
  var asciiConverted;
  var outBase = cmdArgs[0];
  //why does 'undefined' not work???
  if (outBase != undefined) {
    outBase = outBase.toLowerCase();
  }
  if (outBase === "hex") {
   outBase = 16;
 } else if ((outBase === "binary") || (outBase === "bits")) {
   outBase = 2;
  } else if (outBase === "dec"){
   outBase = 10;
  } else if (outBase === "morse") {
   asciiConvertedObj = cTools.asciiToMorse(cmdArgs.slice(1).join(" "));
   //quick fix for major issue #62 ("$asciito morse ("Hello world!")" causes crash)
   //this is because there is no check for failure or sending of failure status in asciiToMorse.js and this file
   //needs cleanup/rework!! but here is a ugly fix
   if (!asciiConvertedObj.hasOwnProperty("unsupportedChars")) {

       console.log("ERROR: asciito morse failure error again!! >.< @ "+ new Date());
       console.log("asciiConvertedObj: "+asciiConvertedObj);
       console.log("typeof asciiConvertedObj: "+typeof asciiConvertedObj);

       console.log("msg:");
       console.log(msg);
       msg.reply("Nice job, you found a bad bug! let the admin know\n\n")
       if (typeof asciiConvertedObj == 'string') {
          msg.reply(asciiConvertedObj)
       }
       return

   }
   if (asciiConvertedObj.unsupportedChars.length >= 1) {
     //give unsupported chars removed in message
     msg.reply("`Ascii to Morse removed the following unsupported Chars: `\n```\n"+asciiConvertedObj.unsupportedChars+"```")
      .then(msg.reply("`Converted from Ascii to Morse: `\n```\n"+asciiConvertedObj.stringOut+"```", {'split': splitOptions}))
      return


   } else {
     msg.reply("`Converted from Ascii to Morse: `\n```\n"+asciiConvertedObj.stringOut+"```", {'split': splitOptions});
     return
   }
  }


  asciiConverted = cTools.asciiToBase(outBase, cmdArgs.slice(1).join(" "));
  let convOutput = "\n`Converted from Ascii to base "+outBase+":`\n```\n"+asciiConverted+"```";
  if (convOutput.length > 1950) {
    msg.sply(" `ERROR`\n```\Ascii Conversion output Larger than message length limit.\n try splitting up input into smaller chunks");
    return
  }
  msg.reply(convOutput);
  return
}
