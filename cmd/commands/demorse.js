const cTools = require('./../../features/cTools/cTools.js');


module.exports = function (db, config, client, msg, command, cmdArgs) {


  var morseConverted = cTools.morseToAscii(cmdArgs.join(" "));

  let convOutput = "\n`Converted from Morse to Ascii :`\n```\n"+morseConverted+"```";
  if (convOutput.length > 1950) {
    msg.reply(" `ERROR`\n```Morse to Ascii Conversion output Larger than message length limit.\n try splitting up input into smaller chunks");
    return
  }
  msg.reply(convOutput);
  return
}
