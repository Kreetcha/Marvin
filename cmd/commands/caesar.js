const cTools = require('./../../features/cTools/cTools.js');


module.exports = function(db, config, client, msg, command, cmdArgs){
  var rot = cmdArgs[0];

  var convOutput;
  //why does 'undefined' not work???
  if (rot != undefined) {
    rot = rot.toLowerCase();
  }

  if (rot === "break") {
    //brute force caesarShift
    var bfResults = "";
    var plainText = cmdArgs.slice(1).join(" ");
    for (var i = 0; i < 26; i++) {
      bfResults += "\("+i+"\):  "+cTools.caesarShift(i, plainText)+"\n\n";
    }
    convOutput = "\n`caesar Shift Brute Force :`\n```\n"+bfResults+"```";
    //pred to send
    var prependString ="\n`caesar Shift Brute Force :`\n```\n"
    msg.reply(convOutput,{split:{prepend: prependString, append: "```"}});
  } else {

    var shiftedString = cTools.caesarShift(rot, cmdArgs.slice(1).join(" "));
    convOutput = "\n`Alphabet rotated by "+rot+" :`\n```\n"+shiftedString+"```";

    //prep to send
    var prependString = "\n`Alphabet rotated by "+rot+" continued :`\n```\n"
    msg.reply(convOutput,{split:{prepend: prependString, append: "```"}});
    return
  }

  // if (convOutput.length > 1950) {
  //   msg.channel.send(msg.author+" `ERROR`\n```\caesar shift output Larger than message length limit.\n try splitting up input into smaller chunks");
  //   return
  // }

  //moved sending msg to conditional DELETE LATER
  // msg.channel.send(convOutput);
  return
}
