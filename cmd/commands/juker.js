const juke = require('../../features/juke/juke.js');




module.exports = function (db, config, client, msg, command, cmdArgs) {

  // juke.inc()
  // msg.reply("val: "+ juke.value()).then(msg.reply("done"));

  // CHECK IF MESSAGE IS IN A SERVER / NOT A DM

  //check if not a dm
  if(  msg.channel.type !== "GUILD_TEXT" || msg.channel.type === null || msg.channel.type === "UNKNOWN" || msg.channel.type === "DM") {
    msg.reply("\nThis command is only for use in guild text channels >.<\nTry `!juke` in a text channel for more info.");
    return;
  }

  if (cmdArgs.length == 0) {
      msg.reply("\nThe juke command is currently in BETA TESTING\nPlease report all bugs and oddities to my admin <3\n\n\`Examples:\`\n\`\`\`!juke YOUTUBE_VIDEO_URL_HERE\n!juke play YOUTUBE_VIDEO_URL_HERE\n!juke skip\n!juke stop\n!juke volume\n!juke volume DESIRED_VOLUME_0-10\n!juke volume up/down\n!juke vol u/d\n!juke v u/d\n!juke list\`\`\`")
      return;
  }

  var jukeCmd = cmdArgs[0];

  //check if url FIRST before we change to lower case

  if (juke.validateURL(jukeCmd)) {
    // THIS IS URL SO PLAY THIS SONG

    var jukeURL = jukeCmd;
    const voiceChannel = msg.member.voice.channel;
    if (!voiceChannel) {
        msg.reply ("\nYou need to join a voice channel to add songs to the queue.")
        return;
    } else {
      const permissions = voiceChannel.permissionsFor(msg.client.user)
      if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
        msg.reply("\nI don't have permission to connect and speak in your voice channel >.<")
      }
    }
		// prep joinVoiceChannel options for juke

		// https://stackoverflow.com/questions/68784950/discord-js-13-channel-join-is-not-a-function
		// https://discordjs.guide/voice/voice-connections.html#cheat-sheet
		// wtf is adapterCreator

//		var voiceChannelInfo = {
//			"channelId": msg.member.voice.channel.id,
//			"guildId": msg.guild.id,
////			"adapterCreator": msg.guild.voiceAdapterCreator
//			"adapterCreator": msg.channel.guild.voiceAdapterCreator
//		}

		var thisVoiceChannel = msg.member.voice.channel
		var voiceChannelInfo = {
			"channelId": thisVoiceChannel.id,
			"guildId": thisVoiceChannel.guild.id,
//			"adapterCreator": msg.guild.voiceAdapterCreator
			"adapterCreator": thisVoiceChannel.guild.voiceAdapterCreator
		}

    juke.execute(msg, jukeURL, voiceChannelInfo);
    return;
  } //wasnt a url :/

  //check for cmd
  jukeCmd = jukeCmd.toLowerCase();
  if (jukeCmd === "play"){
    //check if url was supplied
    if(cmdArgs.length < 2) {
      // msg.reply(" Juke HELP PLACEHOLDER: (1 cmdArgs) ADD YT URL DUH");
      msg.reply("\nPlease supply a URL to a youtube video.\n\n`!juke play REPLACE_WITH_YOUTUBE_URL`");

      return;
    }

    var jukeURL = cmdArgs[1];
    //check if url is valid
    var urlGood = juke.validateURL(jukeURL);
    if (!urlGood){
      msg.reply("\nError: Bad URL >.<\nPlease supply a URL to a youtube video.\n\n`!juke play REPLACE_WITH_YOUTUBE_URL`")
      return;
    }


    const voiceChannel = msg.member.voice.channel;
    if (!voiceChannel) {
        msg.reply ("\nYou need to join a voice channel to add songs to the queue.")
        return;
    } else {
      const permissions = voiceChannel.permissionsFor(msg.client.user)
      if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
        msg.reply("\nI don't have permission to connect and speak in your voice channel >.<")
      }
    }

	var thisVoiceChannel = msg.member.voice.channel
	var voiceChannelInfo = {
		"channelId": thisVoiceChannel.id,
		"guildId": thisVoiceChannel.guild.id,
//			"adapterCreator": msg.guild.voiceAdapterCreator
		"adapterCreator": thisVoiceChannel.guild.voiceAdapterCreator
	}

    juke.execute(msg, jukeURL, voiceChannelInfo);
    return;
  } else if (jukeCmd === "skip") {
    juke.skip(msg);
    return;
  } else if (jukeCmd === "stop") {
    juke.stop(msg);
    return;

  } else if (jukeCmd === "list") {
    let songList = juke.list(msg)

    if (!songList){
      // NEEDS ERROR MESSAGE
      return;
    }
    var strOut = "";


    strOut += "`Queue:`\n";
    strOut += "```\n"
    for (var i = 0; i < songList.length; i++) {
      if (i == 0) { //QUICK SLOPPY ADD NEEDS REWRITING POF
        var thisSongStr = String.fromCharCode(9834)+ " - "+ songList[i] + "\n"; // 9834 = music note
      } else
      {
        var thisSongStr = i + " - "+ songList[i] + "\n";
      }
      strOut += thisSongStr;
    }
    strOut += "```";

    msg.reply(strOut)
    return;
  } else if (jukeCmd === "volume" || jukeCmd === "vol" ||  jukeCmd === "v") {

    if(cmdArgs.length < 2) {
      //volume was not supplied
      var vol = juke.volume(msg);
      msg.reply("\nVolume is currently set to: `" + vol + "/10`");
      return;
    } else {
      //volume was supplied maybe
      var vol = cmdArgs[1];

      //input validation
      if (vol === undefined) {
        msg.reply("Please supply a number from 0-10 or up/down to set the volume\n\n`!juke volume 5`\n`!juke volume up`");
        return;
      }
      // if(isNaN(vol) || !vol) {
      if(isNaN(vol)) {
        // vol was not a number >.<


        // if(typeof(vol) === "string") // SLOPPY QUICK POF NEEDS REWRITE
        // {
          vol = vol.toLowerCase();
        // }

        if(vol === "up" || vol === "u"){
          var maxVol = 10;
          var currentVol = juke.volume(msg);

          if(currentVol >= maxVol)
          {
            // volume already set to max >.<
            msg.reply("Volume cannot be set any higher");
            return;
          } else {
            // OK to increment volume
            var newVol = currentVol + 1;

            //apply volume
            var result = juke.volume(msg, newVol);

            //send user volume or error message
            if (result !== false) {
              msg.reply("\nVolume raised to `"+ result + "/10`");

            } else {
              //error setting volume >.<
              console.log("result: "+result);
              msg.reply("\nERR: juke setting volume failed >.<")
              return;

            }

            return;
          }
          return;
        } else if (vol === "down" || vol === "d") {
          var minVol = 0;
          var currentVol = juke.volume(msg);

          if (currentVol <= minVol) {
            // volume already set to min >.<

            msg.reply("Volume cannot be set any lower");
            return;
          } else {
            // OK to decrement volume
            var newVol = currentVol - 1;

            //apply volume
            var result = juke.volume(msg, newVol);

            //send user volume or error message
            if (result !== false) {
              msg.reply("\nVolume lowered to `"+ result + "/10`");

            } else {
              //error setting volume >.<

              msg.reply("\nERR: juke setting volume failed >.<")
              return;

            }


            // msg.reply("\nVolume is now set to: `" + newVol + "/10`");
            return;
          }
          return;
        } else {
          //not a known input
          msg.reply("Please supply a number from 0-10 or up/down to set the volume\n\n`!juke volume 5`\n`!juke volume up`");
          return;
        }

      } else {
        // vol IS a number
        vol = parseInt(vol);

        if(isNaN(vol)) { // unnecessary ??
          //error parsing int
          msg.reply("Error parsing volume value to number >.<\nPLEASE @mention the admin about this<3\n\nSupply a number from 0-10 or up/down to set the volume\n\n`!juke volume 5\n!juke volume up`")
          return;
        }

        //verify range
        if(vol > 11 || vol < 0) {
          msg.reply("The volume setting you supplied is out of range >.<\nPlease supply a number from 0-10 or up/down to set the volume\n\n`!juke volume 5`\n`!juke volume up`")
          return;
        }

        //range OK
        var result = juke.volume(msg, vol);
        if (result !== false) {
          msg.reply("\nVolume set to `"+ result + "/10`");

        } else {
          //error setting volume >.<

          msg.reply("\nERR: juke setting volume failed >.<")
          return;

        }
      }

    }
    return;
  } else if (jukeCmd === "remove") {

    if (cmdArgs.length < 2) { // song to remove was not supplied
      msg.reply("`THIS COMMAND IS IN BETA:`\nReport all errors and oddities to my admin please <3\n\nSupply a track index from 1+ to remove it from the playlist");
      return;
    }

    var indexToRemove = cmdArgs[1];
    if (isNaN(indexToRemove)) { //not a number :(
      msg.reply("Error parsing track number. (NOT A NUMBER)\n\n Please supply a track number from 1 to the last track on the playlist or skip the currently playing track"); // MESSAGES NEED REWORK
      return;
    }

    indexToRemove = parseInt(indexToRemove);


    if( indexToRemove < 1 | indexToRemove > 1000) { // < 1 | SLOPPY QUICK MAX INT CHECKING
      msg.reply("Error parsing track number. (OUT OF RANGE)\n\n Please supply a track number from 1 to the last track on the playlist or skip the currently playing track"); // MESSAGES NEED REWORK
      return;
    }

    var result = juke.remove(msg, indexToRemove);
    return;
  } else if (jukeCmd === "playlist") {

	if (msg.author.id !== config.adminId) {
		msg.reply("This feature is admin only for the time being :(");
		return;
	}

	if (cmdArgs.length < 2) { // song to remove was not supplied
		msg.reply("`THIS COMMAND IS IN BETA:`\nReport all errors and oddities to my admin please <3\n\nSupply a pure playlist URL\n NOT A VIDEO in a playlist @.@");
		return;
	}

	var thisVoiceChannel = msg.member.voice.channel
	var voiceChannelInfo = {
		"channelId": thisVoiceChannel.id,
		"guildId": thisVoiceChannel.guild.id,
//			"adapterCreator": msg.guild.voiceAdapterCreator
		"adapterCreator": thisVoiceChannel.guild.voiceAdapterCreator
	}

	var playlistURL = cmdArgs[1];

	var linkType = juke.getLinkType(playlistURL);

	console.log(playlistURL);
	console.log(linkType);
	if(linkType.playlist == true && linkType.video == true) {
		// video in playlist url
		msg.reply(`The URL you supplied was for a video in a playlist >.<\n Please supply a pure playlist URL`);
	} else if (linkType.playlist == true && linkType.video == false) {
		// pure playlist url
		juke.playPlaylist(msg, playlistURL, voiceChannelInfo)
	} else {
		// not a playlist url (no need to parse for video url?)
		msg.reply("The URL you supplied is not a playlist url >.<");
	}

	//var result = juke.remove(msg, indexToRemove);
	return;
  } else {

    msg.reply("\nJuke Error: Unknown Juke command (or bad URL) >.<\n\nTry `!juke` for a list of examples");
    // console.log("ERR: unkown juke cmd\ncmdArgs:");
    // console.log(cmdArgs);
    // console.log("jukeCmd: "+ jukeCmd);
    return;
  }



  // msg.channel.send("<@"+msg.author.id+'> pong! The websocket heartbeat is '+Math.round(client.ws.ping)+"ms.");
  return;
}
