module.exports = function (db, config, client, msg, command, cmdArgs) {

  if (cmdArgs.length < 1) {
    msg.reply("Please supply a UNIX Epoch timestamp to covert to human readable format.");
    return
  }
  var timeStamp = cmdArgs[0]
  if (isNaN(timeStamp)) {
    msg.reply("Input is not a valid number!");
    return
  }
  timeStamp = Number(timeStamp)
  // var humanReadableTime = newDate(timeStamp).toGMTString()
  var humanReadableTime = new Date(timeStamp).toLocaleString()
  msg.reply(timeStamp+" = `"+humanReadableTime+" CST (UTC-6)`")
  return
}
