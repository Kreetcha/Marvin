module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (msg.author.id !== config.adminId) return;

  msg.reply(":loudspeaker: Taking a roll call...").then(() => {
    if (cmdArgs[0] == "test") {

      require('./../../features/rollCall/rollCall.js').doRollCall(config, client)
    } else {
      require('./../../features/rollCall/rollCall.js').doRollCall(config, client, msg)

    }

  });
  return
}
