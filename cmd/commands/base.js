const cTools = require('./../../features/cTools/cTools.js');

module.exports = function(db, config, client, msg, command, cmdArgs){
  var baseConverted;
  var inBase = cmdArgs[0]
  var outBase = cmdArgs[1];
  //why does 'undefined' not work???
  if (outBase != undefined) {
    outBase = outBase.toLowerCase();
  }
 if (outBase === "hex") {
  outBase = 16;
} else if (outBase === "binary" || outBase === "bits") {
  outBase = 2;
}

if (inBase === "hex") {
 inBase = 16;
} else if (inBase === "binary" || inBase === "bits") {
 inBase = 2;
}

if (outBase === "ascii") {
  baseConverted = cTools.baseToAscii(inBase, cmdArgs.slice(2).join(" ").replace(/(?:\r\n|\r|\n|\t)/g, " "))
} else {
  baseConverted = cTools.baseToBase(inBase, outBase, cmdArgs.slice(2).join(" ").replace(/(?:\r\n|\r|\n|\t)/g, " "));
}
//is .replace(/(?:\r\n|\r|\n)/g, " ") unessesary?

let baseOutput = "\n`Converted from base "+cmdArgs[0]+" to "+cmdArgs[1]+":`\n```\n"+baseConverted+"```";
if (baseOutput.length > 1950) {
  msg.reply("`ERROR`\n```\nBase Conversion output Larger than message length limit.\n try splitting up input into smaller chunks");
  return
}
msg.reply(baseOutput);
return

}
