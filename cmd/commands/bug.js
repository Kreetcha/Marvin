module.exports = function (db, config, client, msg, command, cmdArgs) {
  //send bug report to admin
  //add blank bug report prevention here
  var bugMsg = cmdArgs.join(" ");
  var bugReport = msg.author+" issued a `Bug Report!`\n```"+bugMsg+"```";
  var bugReportCopy = msg.author+", here is a copy of your recently issued `Bug Report!`\n```"+bugMsg+"```";
  client.users.get(config.adminId).send(bugReport);
  msg.author.send(bugReportCopy);
 return
}
