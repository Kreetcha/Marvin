module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (typeof db == "undefined") {
    console.log("db not connected (undefined)!!")
    console.log("ERROR: error retrieving myreminds @ "+new Date());
    msg.author.send(msg.author+"db not connected ):\n Please retry or contact admin if this persists")
    return
  }
  db.collection('reminders').find({"recipientId":msg.author.id}).toArray ((err,result) => {
    if (err) {
      console.log("ERROR: error retrieving reminders @ "+new Date());
      console.log(err);
      msg.author.send(msg.author+"error retrieving your reminders:\n Please retry or contact admin if this persists")
      return
    }
    var reminders = result.sort((a, b) => a.timeToRemind - b.timeToRemind);
    outText = "__Your Reminders__\n\n"
    for (reminder of reminders){
      // console.log(reminder);
      var timeTillReminder = reminder.timeToRemind-Date.now()
      // var dateTill = new Date(null);
      // dateTill.setSeconds(timeTillReminder/1000);
      // var dateHumanReadable = dateTill.toISOString().substr(11, 8);

      var tillTimeSeconds = timeTillReminder/1000
      var tillDays = Math.floor(tillTimeSeconds / 86400);
      var tillHours = Math.floor((tillTimeSeconds - (tillDays * 86400)) / 3600);
      var tillMinutes = Math.floor((tillTimeSeconds - ((tillHours * 3600) + (tillDays * 86400)))  / 60);
      var tillSeconds = Math.floor(tillTimeSeconds - ((tillDays * 86400) + (tillHours * 3600) + (tillMinutes * 60)) )

     //  var upH = Math.floor(uptimeSeconds / (6060));
     // var upM = Math.floor(uptimeSeconds % (6060) / 60);
     // var upS = Math.floor(uptimeSeconds % 60);
     // var uptimeStr = upDays+"days "+upHours+"h "+upMinutes+"m "+upSeconds+"s"
     var timeTillString = ""
     if (tillDays > 0) {
       timeTillString += tillDays+"days "
     }
    timeTillString += tillHours+"h "+tillMinutes+"m "+tillSeconds+"s"
      outText+= "`"+reminder.reminderTxt+"` in "+timeTillString+"\n"
    }
    msg.author.send(outText)
  });
  // msg.channel.send(msg.author+' pong! The websocket heartbeat is '+Math.round(client.ping)+"ms.");
  return
}
