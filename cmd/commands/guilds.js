module.exports = function (db, config, client, msg, command, cmdArgs) {
  if (msg.author.id !== config.adminId) return;
  console.log("Running admin command (guilds) @ "+new Date());
  var guildNames = []
  client.guilds.cache.forEach((guild) =>{guildNames.push(guild.name);})
  output = "__**Guilds**__\n\n"
  for (name of guildNames){
    output += "`"+name+"`\n"
  }
  msg.channel.send(output);
  return
}
