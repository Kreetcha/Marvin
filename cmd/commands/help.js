const helpHandler = require('./../../features/help/helpHandler.js');

module.exports = function(db, config, client, msg, command, cmdArgs){
  if (msg.channel.type != "DM") {
    msg.channel.send('<@' + msg.author.id + '> I\'d give you advice, but you wouldn\'t listen.\n No one ever does...');
  }
  //DM user with helplist of commands here

  helpHandler(msg,cmdArgs);
  //msg.author.send(helpMsgs.mainHelpEmbed);
  return
}
