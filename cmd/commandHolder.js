module.exports.help = require("./commands/help.js");
module.exports.ping = require("./commands/ping.js");
//module.exports.bug = require("./commands/bug.js");
//module.exports.suggest = require("./commands/suggest.js");
module.exports.eval = require("./commands/eval.js");
module.exports.d = require("./commands/d.js");
module.exports.remind = require("./commands/remind.js");
module.exports.base = require("./commands/base.js");
module.exports.split = require("./commands/split.js");
module.exports.asciito = require("./commands/asciito.js");
module.exports.cshift = require("./commands/caesar.js");
module.exports.demorse = require("./commands/demorse.js");
module.exports.ioc = require("./commands/ioc.js");
//module.exports.shift = require("./commands/shift.js");
module.exports.epoch = require("./commands/epoch.js");

module.exports.stats = require("./commands/stats.js");
module.exports.guilds = require("./commands/guilds.js");
module.exports.myreminds = require("./commands/myreminds.js");
//module.exports.rtest = require("./commands/rtest.js");
module.exports.channels = require("./commands/channels.js");
module.exports.rtest2 = require("./commands/rtest2.js");
//module.exports.dorollcall = require("./commands/dorollcall.js");
//module.exports.rollcallignore = require("./commands/rollcallignore.js");

module.exports.test = require("./commands/test.js");

module.exports.birdhouse = require("./commands/birdhouse.js");
module.exports.birdhouses = require("./commands/birdhouse.js");
module.exports.herbrun = require("./commands/herbrun.js");


module.exports.juke = require("./commands/juker.js");
