
const commandHolder = require('./commandHolder.js');


module.exports = function(db, config, client, msg, command, cmdArgs){
  // check if there is a command with that name in the command holder
  if (commandHolder.hasOwnProperty(command)){
    // execute that command

    commandHolder[command](db, config, client, msg, command, cmdArgs);
  }
  return
}
